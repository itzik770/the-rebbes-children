import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  helpersCollection: AngularFirestoreCollection<{}>;
  constructor(private afs: AngularFirestore) {
    this.helpersCollection = this.afs.collection<{}>('helpers');
  }

  // increseCounter(){
  //   this.helpersCollection.doc(`123`)
  // }
}
