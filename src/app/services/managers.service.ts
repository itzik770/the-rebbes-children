import { Injectable } from '@angular/core';
import { Manager } from '../models/Manager';
import { AngularFirestoreCollection, AngularFirestore, DocumentReference, DocumentChangeAction } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { first, take } from 'rxjs/operators';
// import { take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ManagersService {

  managersCollection: AngularFirestoreCollection<Manager>;
  constructor(private afs: AngularFirestore) {
    this.managersCollection = this.afs.collection<Manager>('managers');
  }
  // getManagers(): Observable<{}> {
  //   return this.managersCollection.valueChanges();
  // }

  getManagers(): Promise<DocumentChangeAction<Manager>[]> {
    // return this.managersCollection.snapshotChanges().pipe(take(1)).toPromise();
    return this.afs.collection<Manager>('managers').snapshotChanges().pipe(take(1)).toPromise();
  }

  getManagerById(managerId: string): Promise<Manager> {
    return this.managersCollection.doc<Manager>(`${managerId}`).valueChanges().pipe(first()).toPromise();
  }

  getManagerByUid(firebaseUid: string): Promise<DocumentChangeAction<Manager>[]> {
    return this.afs.collection<Manager>('managers', ref => ref.where('firebaseUid', '==', firebaseUid))
      .snapshotChanges().pipe(first()).toPromise();
    // return this.afs.collection<Student>(`students`, ref => ref.where('classId', '==', classId)).valueChanges();
  }

  addManager(managerToAdd: Manager): Promise<DocumentReference> {
    return this.managersCollection.add(managerToAdd);
  }

  increaseStudentCounter(managerId: string) {
    this.managersCollection.doc(managerId).update({ 'studentCounter': firebase.firestore.FieldValue.increment(1) });
  }
  increaseClassCounter(managerId: string) {
    this.managersCollection.doc(managerId).update({ 'classCounter': firebase.firestore.FieldValue.increment(1) });
  }

  increaseProductCounter(managerId: string) {
    this.managersCollection.doc(managerId).update({ 'productCounter': firebase.firestore.FieldValue.increment(1) });
  }

  increaseCampaignCounter(managerId: string) {
    this.managersCollection.doc(managerId).update({ 'campaignCounter': firebase.firestore.FieldValue.increment(1) });
  }

  updateManager(managerToUpdate: Manager): Promise<any> {
    return this.managersCollection.doc(managerToUpdate.id).update(managerToUpdate);
  }

  deleteManager(managerToDelete: Manager): Promise<any> {
    return this.managersCollection.doc(managerToDelete.id).delete();
  }
}
