import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { Campaign } from '../models/Campaign';
import { Observable } from 'rxjs';
import { Manager } from '../models/Manager';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CampaignsService {

  campaignsCollection: AngularFirestoreCollection<Campaign>;
  constructor(private afs: AngularFirestore) {
    this.campaignsCollection = this.afs.collection<Campaign>('campaigns');
  }

  addCampaign(newCampaign: Campaign, managerInfo: Manager): string {
    const docId = managerInfo.managerCode + managerInfo.campaignCounter;
    this.campaignsCollection.doc(docId).set(newCampaign);
    return docId;
  }

  getCampaignById(campaignId: string): Observable<Campaign> {
    return this.campaignsCollection.doc<Campaign>(`${campaignId}`).valueChanges();
  }
  getCampaignsUnderStudentId(studentId: string): Observable<DocumentChangeAction<Campaign>[]> {
    return this.afs.collection<Campaign>(`campaigns`, ref => ref.where('studentId', '==', studentId)).snapshotChanges();
  }

  getCampaignsUnderSchoolId(schoolId: string): Observable<DocumentChangeAction<Campaign>[]> {
    return this.afs.collection<Campaign>(`campaigns`, ref => ref.where('schoolId', '==', schoolId))
      .snapshotChanges();
  }
}
