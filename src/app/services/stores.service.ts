import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, DocumentChangeAction, DocumentReference, Action, DocumentSnapshot } from 'angularfire2/firestore';
import { Store } from '../models/Store';
import { Observable } from 'rxjs';
import { Product } from '../models/Product';
import { promise } from 'protractor';
import { take } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class StoresService {

  storesCollections: AngularFirestoreCollection<Store>;

  constructor(private afs: AngularFirestore) {
    this.storesCollections = this.afs.collection<Store>('stores');
  }
  getStores(): Promise<DocumentChangeAction<Store>[]> {
    return this.afs.collection<Store>('stores').snapshotChanges().pipe(take(1)).toPromise();
  }
  addStore(store: Store): Promise<void> {
    return this.storesCollections.doc(store.id).set(store);
  }
  getStoreById(storeId: string): Promise<Action<DocumentSnapshot<Store>>> {
    return this.storesCollections.doc<Store>(`${storeId}`).snapshotChanges().pipe(take(1)).toPromise();;
    // return this.storesCollections.doc<Store>(`${storeId}`).valueChanges();
  }
  increasePurchasesCounter(storeId: string) {
    this.storesCollections.doc(storeId).update({ 'numberOfPurchase': firebase.firestore.FieldValue.increment(1) });
  }
  updateStore(storeToUpdate: Store): Promise<any> {
    return this.storesCollections.doc(storeToUpdate.id).update(storeToUpdate);
  }
  addProductToStore(storeId: string, productToAdd: Product): Promise<any> {
    return this.storesCollections.doc(`${storeId}`).update({ products: productToAdd });
  }
  deleteStore(StoreToDelete: Store): Promise<any> {
    return this.storesCollections.doc(StoreToDelete.id).delete();
  }

  // updateProduct(storeId: string, productToUpdate: Product): Promise<any> {
  //   let products;
  //   this.storesCollections.doc(`${storeId}`).get().subscribe(store => {
  //     const data = store.data();
  //     products = data.products;
  //   });
  //   // store.
  //   // return this.storesCollections.doc(`${storeId}`).update({ products: ...products, productToUpdate });
  // }
  deleteProductFromStore(storeId: string, productToDelete: Product): Promise<any> {
    return this.storesCollections.doc(`${storeId}`).delete();
  }
}
