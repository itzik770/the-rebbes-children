import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentReference,
  AngularFirestoreDocument,
  DocumentSnapshot,
  DocumentChangeAction,
  Action
} from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { Student } from '../models/Student';
import { PassToClassDialog } from '../models/PassToClassDialog';
import { CampaignScore } from '../models/CampaignScore';
import { Class } from '../models/Class';
import { take, first, map } from 'rxjs/operators';
// import { Manager } from '../models/Manager';
// import * as firebase from 'firebase';
import { ManagersService } from './managers.service';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  students: Student[];
  studentsCollections: AngularFirestoreCollection<Student>;
  // managersCollection: AngularFirestoreCollection<Manager>;

  // classesCollections: AngularFirestoreCollection<Class>;
  studentDoc: AngularFirestoreDocument<Student>;
  student: Observable<Student>;

  constructor(private afs: AngularFirestore, private managersService: ManagersService) {

    this.studentsCollections = this.afs.collection<Student>('students');
    // this.managersCollection = this.afs.collection<Manager>('managers');
  }

  getStudents():Promise<DocumentChangeAction<Student>[]>{
    return this.studentsCollections.snapshotChanges().pipe(take(1)).toPromise();
  }

  // MUST
  getStudentById(studentId: string): Observable<Action<DocumentSnapshot<Student>>> {
    // : Promise<Student> {{}
    // const ref = this.afs.collection<Student>('students').valueChanges({ idField: 'id' });
    // return ref.valueChanges({ idField: 'id' });
    return this.afs.collection<Student>('students').doc<Student>(`${studentId}`).snapshotChanges().pipe(first());
    // return this.studentsCollections.doc<Student>(`${studentId}`).valueChanges();
  }

  getStudentByFirstAndLastName(firstName: string, lastName: string, classId: string): Observable<DocumentChangeAction<Student>[]> {
    return this.afs.collection<Student>('students', ref => ref
      .where('lastName', '==', lastName)
      .where('firstName', '==', firstName)
      .where('classId', '==', classId))
      .snapshotChanges();
  }

  getStudentsUnderClass(classId: string): Observable<DocumentChangeAction<Student>[]> {
    // console.log(classId);

    return this.afs.collection<Student>(`students`, ref => ref.where('classId', '==', classId)).snapshotChanges();
  }

  getStudentsUnderSchool(schoolId: string): Observable<DocumentChangeAction<Student>[]> {
    return this.afs.collection<Student>(`students`, ref => ref.where('schoolId', '==', schoolId)).snapshotChanges();
  }

  updateStudent(studentToUpdate: Student): Promise<any> {
    return this.studentsCollections.doc(`${studentToUpdate.id}`).update(studentToUpdate);
  }

  // async addStudent(studentToAdd: Student, importantInfo: PassToClassDialog): Promise<string> {
  //   const manager = await this.managersService.getManagerById(importantInfo.managerInfo.id);
  //   const docId = manager.managerCode + importantInfo.parentId.substring(3) + manager.studentCounter;
  //   this.studentsCollections.doc(docId).set(studentToAdd);
  //   return docId;
  // }

  async addStudent(studentToAdd: Student) {
    // async addStudent(studentToAdd: Student, importantInfo: PassToClassDialog) {
    // const manager = await this.managersService.getManagerById(importantInfo.managerInfo.id);
    // const docId = manager.managerCode + importantInfo.parentId.substring(3) + manager.studentCounter;
    const checkIfStudentIdExists = await this.studentsCollections.doc(studentToAdd.id).get().toPromise();
    // console.log('checkIfStudentIdExist', checkIfStudentIdExists);
    if (checkIfStudentIdExists.exists) {
      throw "student id exists";
    } else {
      return this.studentsCollections.doc(studentToAdd.id).set(studentToAdd);
    }
    // return docId;
  }

  async getAllStudentsUnderSchool(schoolId: string) {
    const students = this.afs.collection<Student>('students', ref => ref.where('schoolId', '==', schoolId))
      .snapshotChanges().pipe(take(1)).toPromise();
    return students;
  }

  async orderStudents(students: DocumentChangeAction<Student>[]) {
    const orderStudents: Student[] = [];
    for (const studentData of students) {
      const data = await studentData.payload.doc.data();
      const id = await studentData.payload.doc.id;
      await orderStudents.push({ id, ...data });
    }
    return orderStudents;
  }
  async addCampaignToStudentsUnderSchool(campaignId: string, campaignName: string, schoolId: string) {
    const students = await this.getAllStudentsUnderSchool(schoolId);
    const orderedStudents = await this.orderStudents(students);
    for (let index = 0; index < orderedStudents.length; index++) {
      const student = orderedStudents[index];
      const studentCampaigns: CampaignScore[] = await student.campaignScores || [];
      await studentCampaigns.push({ campaignId: campaignId, campaignName: campaignName, earnedPoints: 0, spandPoints: 0 });
      this.studentsCollections.doc(student.id).update({
        'campaignScores': studentCampaigns
      });

    }
  }

  async addCampaignToStudent(student: Student, campaignId: string, campaignName: string) {
    const studentCampaigns: CampaignScore[] = await student.campaignScores || [];
    await studentCampaigns.push({ campaignId: campaignId, campaignName: campaignName, earnedPoints: 0, spandPoints: 0 });
    await this.studentsCollections.doc(student.id).update({
      'campaignScores': studentCampaigns
    });
  }

  async addPointFieldUnderStudent(student: Student) {
    const studentPoints = student.campaignScores[0].earnedPoints;
    console.log('Old Student', student["earnedPoints"]);

    student["earnedPoints"] = studentPoints;
    console.log('New Student', student["earnedPoints"]);
    
    await this.studentsCollections.doc(student.id).update(student);
  }
  addStudentUrlPic(studentId: string, downloadUrl: string): Promise<void> {
    return this.studentsCollections.doc(studentId).update({ 'pictureUrl': downloadUrl });
  }

  async addStudentScore(studentId: string, campaignId: string, amountToAdd: number) {
    const studentPromise = await this.getStudentById(studentId).toPromise();
    const studentData: Student = studentPromise.payload.data();
    studentData.id = studentData.id;
    studentData.campaignScores.find(campaign => campaign.campaignId === campaignId).earnedPoints += amountToAdd;
    studentData.id = studentId;
    studentData.earnedPoints += amountToAdd;
    this.updateStudent(studentData);
  }

  async subStudentScore(studentId: string, campaignId: string, amountToSub: number) {
    const studentPromise = await this.getStudentById(studentId).toPromise();
    const studentData: Student = studentPromise.payload.data();
    studentData.id = studentData.id;
    console.log(studentData);
    
    studentData.campaignScores.find(campaign => campaign.campaignId === campaignId).earnedPoints -= amountToSub;
    studentData.id = studentId;
    console.log(studentData);
    this.updateStudent(studentData);
  }

  async increaseStudentSpandAmount(studentId: string, campaignId: string, amountToAdd: number) {
    const studentPromise = await this.getStudentById(studentId).toPromise();
    const studentData: Student = await studentPromise.payload.data();
    studentData.id = await studentData.id;
    studentData.campaignScores.find(campaign => campaign.campaignId === campaignId).spandPoints += amountToAdd;
    studentData.id = studentId;
    this.updateStudent(studentData);
  }

  getTopStudentsManagerView(schoolId: string, field: string, limit: number): Promise<DocumentChangeAction<Student>[]> {
    return this.afs.collection<Student>('students', ref =>
      ref
        .where('schoolId', '==', schoolId)
        .orderBy(field, 'desc')
        .limit(limit)
    ).snapshotChanges().pipe(take(1)).toPromise();
  }

  deleteStudent(studentId: string): Promise<any> {
    return this.studentsCollections.doc(studentId).delete();
  }

  async deleteStudentUnderClass(classId: string) {
    const studentsToDeleteRef = this.afs.collection<Student>(`students`, ref => ref.where('classId', '==', classId));
    studentsToDeleteRef.get().subscribe(querySnapshot => {
      querySnapshot.forEach(doc => {
        doc.ref.delete();
      });
    });
  }

  // async changeStudentId(studentInfo: Student, newStudentId: string) {
  //   console.log('studentInfo', studentInfo);
  //   console.log('newStudentId', newStudentId);
    
  //   const newStudent = studentInfo;
  //   newStudent.id = newStudentId;
  //   console.log('newStudent', newStudent);

  //   // await this.studentsCollections.doc(newStudentId).set(newStudent);
  //   await this.addStudent(newStudent);
  //   // await this.studentsCollections.doc(studentInfo.id).delete();
  // }
}
