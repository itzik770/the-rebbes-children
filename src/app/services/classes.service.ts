import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentChangeAction, DocumentReference, Action, DocumentSnapshot } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Class } from '../models/Class';
import { Manager } from '../models/Manager';
import * as firebase from 'firebase/app';
import { School } from '../models/School';
import { Student } from '../models/Student';
import { ManagersService } from './managers.service';
import { StudentList } from '../models/StudentList';
// import { firebase } from '@firebase/*';

@Injectable({
  providedIn: 'root'
})
export class ClassesService {

  classesCollection: AngularFirestoreCollection<Class>;
  schoolsCollection: AngularFirestoreCollection<School>;
  constructor(private afs: AngularFirestore, private managersService: ManagersService) {
    this.classesCollection = this.afs.collection<Class>('classes');
    this.schoolsCollection = this.afs.collection<School>('schools');
  }
  getClasses(): Observable<Class[]> {
    return this.classesCollection.valueChanges();
  }

  getClassById(classId: string): Promise<Action<DocumentSnapshot<Class>>> {
    if (classId === '') {
      classId = 'bla';
    }
    return this.classesCollection.doc<Class>(`${classId}`)
      .snapshotChanges()
      .pipe(take(1))
      .toPromise();;
  }
  getClassesBySchoolId(schoolId: string): Promise<DocumentChangeAction<Class>[]> {
    return this.afs.collection<Class>(`classes`, ref => ref.where('schoolId', '==', schoolId))
      .snapshotChanges()
      .pipe(take(1)).toPromise();
  }

  async addClass(managerInfo: Manager, classToAdd: Class) {
    const manager = await this.managersService.getManagerById(managerInfo.id);
    const docId = manager.managerCode + manager.classCounter.toString();
    this.classesCollection.doc(docId).set(classToAdd);
    return docId;
  }

  addStudentToClass(studentId: string, classId: string): Promise<void> {
    return this.classesCollection.doc(classId).update({ 'studentList': firebase.firestore.FieldValue.arrayUnion(studentId) });
  }

  addStudentToClassWithName(studentInfo: Student): Promise<void> {
    const newStudent = {} as StudentList;
    newStudent.id = studentInfo.id
    newStudent.name = `${studentInfo.lastName} ${studentInfo.firstName}`
    return this.classesCollection.doc(studentInfo.classId).update({
      'studentList': firebase.firestore.FieldValue.arrayUnion(newStudent)
    });
  }

  removeStudentFromClass(studentToDelete: Student): Promise<void> {
    // console.log({ 'id': studentToDelete.id, 'name': `${studentToDelete.firstName} ${studentToDelete.lastName}` });

    return this.classesCollection.doc(studentToDelete.classId)
      .update({ 'studentList': firebase.firestore.FieldValue.arrayRemove({ 'id': studentToDelete.id, 'name': `${studentToDelete.firstName} ${studentToDelete.lastName}` }) });
  }

  // async changeStudentId(studentInfo: Student, newStudentId: string) {
  //   await this.removeStudentFromClass(studentInfo);
  //   await this.addStudentToClassWithName(newStudentId, studentInfo.classId, `${studentInfo.firstName} ${studentInfo.lastName}`);
  // }

  async changeStudentNameInClassList(oldStudentInfo: Student, newStudentInfo: Student) {
    // console.log('changeStudentNameInClassList');

    await this.removeStudentFromClass(oldStudentInfo);
    await this.addStudentToClassWithName(newStudentInfo);
  }

  updateClass(classToUpdate: Class): Promise<any> {
    return this.classesCollection.doc(classToUpdate.id).update(classToUpdate);
  }
  deleteClass(classId: string): Promise<any> {
    return this.classesCollection.doc(classId).delete();
  }
}
