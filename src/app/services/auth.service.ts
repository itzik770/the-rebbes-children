import { Injectable, NgZone, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
// import * as firebase from 'firebase';
import * as firebase from 'firebase/app';
import { StorageService, LOCAL_STORAGE } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: Observable<firebase.User>;
  STORAGE_KEY = 'manager_info';
  constructor(public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone,
    @Inject(LOCAL_STORAGE) public storage: StorageService) { }

  // doFacebookLogin() {
  //   return new Promise<any>((resolve, reject) => {
  //     const provider = new firebase.auth.FacebookAuthProvider();
  //     this.afAuth.auth
  //       .signInWithPopup(provider)
  //       .then(res => {
  //         resolve(res);
  //       }, err => {
  //         console.log(err);
  //         reject(err);
  //       });
  //   });
  // }

  // doTwitterLogin() {
  //   return new Promise<any>((resolve, reject) => {
  //     const provider = new firebase.auth.TwitterAuthProvider();
  //     this.afAuth.auth
  //       .signInWithPopup(provider)
  //       .then(res => {
  //         resolve(res);
  //       }, err => {
  //         console.log(err);
  //         reject(err);
  //       });
  //   });
  // }
  // doGoogleLogin() {
  //   return new Promise<any>((resolve, reject) => {
  //     const provider = new firebase.auth.GoogleAuthProvider();
  //     console.log(JSON.stringify(provider));

  //     provider.addScope('profile');
  //     provider.addScope('email');
  //     this.afAuth.auth
  //       .signInWithPopup(provider)
  //       .then(res => {
  //         resolve(res);
  //       }, err => {
  //         console.log(err);
  //         reject(err);
  //       });
  //   });
  // }

  doRegister(value) {
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(value.email, value.password)
        .then(result => {
          this.sendVerificationMail();
          resolve(result);
        }, err => reject(err));
    });
  }

  sendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.router.navigate(['/']);
      });
  }

  doLogin(email: string, password: string) {
    console.log('doLogin email', email);
    console.log('doLogin password', password);

    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(result => {
          // if (result.user.emailVerified !== true) {
          //   this.sendVerificationMail();
          //   window.alert('Please validate your email address. Kindly check your inbox.');
          // } 
          //       else {
          console.log('doLogin success');

          this.ngZone.run(() => {
            this.router.navigate(['/manager_info']);
          });
          // }
          resolve(result);
        }, err => { 
          console.log(err);
          
          reject(err) });
    });
  }

  doLogout() {
    return new Promise((resolve, reject) => {
      if (firebase.auth().currentUser) {
        this.afAuth.auth.signOut();
        this.storage.remove(this.STORAGE_KEY);
        resolve();
      } else {
        // reject();
        console.log('No user to logged out');

      }
    });
  }
}
