import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, DocumentChangeAction, QueryDocumentSnapshot } from 'angularfire2/firestore';
import { Score } from '../models/Score';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ScoreService {

  scoresCollections: AngularFirestoreCollection<Score>;
  constructor(private afs: AngularFirestore) {
    this.scoresCollections = this.afs.collection<Score>('scores');
  }
  getAllScoreUnderStudentAndCampaign(studentId: string, campaignId: string): Observable<Score[]> {
    return this.afs.collection<Score>('scores', ref => ref.where('studentId', '==', studentId).where('campaignId', '==', campaignId))
      .valueChanges();
  }

  getAllScoreUnderStudent(studentId: string): Promise<Score[]> {
    return this.afs.collection<Score>('scores', ref => ref.where('studentId', '==', studentId).orderBy('scoreDate', 'desc'))
      .valueChanges().pipe(take(1)).toPromise();
  }

  async changeStudentIdInAllTheScoresUnderStudent(oldStudentId: string, newStudentId: string) {
    const querySnapshot = await this.afs.collection<Score>('scores', ref => ref.where('studentId', '==', oldStudentId)).get().toPromise();
    querySnapshot.forEach(doc => {
      let copyOfTheScore: Score = doc.data() as Score;
      copyOfTheScore.id = doc.id;
      // console.log('score before', copyOfTheScore);
      
      copyOfTheScore.studentId = newStudentId;
      // console.log('score after', copyOfTheScore);

      this.scoresCollections.doc(copyOfTheScore.id).update(copyOfTheScore);
    });
  }

  async getFirstScoresUnderStudent(studentId: string): Promise<Score[]> {
    // console.log(studentId);
    
    return this.afs.collection<Score>('scores', ref => ref.where('studentId', '==', studentId).orderBy('scoreDate', 'desc')
      .limit(5)).valueChanges().pipe(take(1)).toPromise();
  }

  addScore(scoreToAdd: Score) {
    return this.scoresCollections.add(scoreToAdd);
  }

  getFirstScoresUnderStudentManagerView(studentId: string, field: string, limit: number): Promise<DocumentChangeAction<Score>[]> {
    return this.afs.collection<Score>('scores', ref =>
      ref
        .where('studentId', '==', studentId)
        .orderBy(field, 'desc')
        .limit(limit)
    ).snapshotChanges().pipe(take(1)).toPromise();
  }

  getMore(studentId: string, field: string, limit: number, docTemp: QueryDocumentSnapshot<Score>): Promise<DocumentChangeAction<Score>[]> {
    return this.afs.collection<Score>('scores', ref =>
      ref
        .where('studentId', '==', studentId)
        .orderBy(field, 'desc')
        .limit(limit)
        .startAfter(docTemp)
    ).snapshotChanges().pipe(take(1)).toPromise();
  }

  deleteScore(scoreId: string) {
    this.afs.collection<Score>('scores').doc(scoreId).delete();
  }
}
