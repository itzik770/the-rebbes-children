import { Injectable } from '@angular/core';
import { Order } from '../models/Order';
import { AngularFirestoreCollection, AngularFirestore, DocumentChangeAction, QueryDocumentSnapshot } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { resolve } from 'path';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  ordersCollection: AngularFirestoreCollection<Order>;
  constructor(private afs: AngularFirestore) {
    this.ordersCollection = this.afs.collection<Order>('orders');
  }

  addOrder(orderToAdd: Order): Promise<any> {
    return this.ordersCollection.add(orderToAdd);
  }

  getOrders(): Observable<Order[]> {
    return this.ordersCollection.valueChanges();
  }

  async getOrdersUnderStudentAndCampaign(studentId: string, campaignId: string): Promise<Order[]> {
    return this.afs.collection<Order>('orders', ref => ref.where('studentId', '==', studentId).where('campaignId', '==', campaignId))
      .valueChanges().pipe(take(1)).toPromise();
  }

  async getFirstOrdersUnderStudent(studentId: string): Promise<Order[]> {
    return this.afs.collection<Order>('orders', ref => ref.where('studentId', '==', studentId).orderBy('purchaseDate', 'desc')
      .limit(5)).valueChanges().pipe(take(1)).toPromise();
  }

  getOrdersUnderStudent(studentId: string): Promise<Order[]> {
    return this.afs.collection<Order>('orders', ref => ref.where('studentId', '==', studentId))
      .valueChanges()
      .pipe(take(1))
      .toPromise();
  }

  getAllUnfinishedOrdersBySchool(schoolId: string) {
    return this.afs.collection<Order>('orders', ref => ref.where('schoolId', '==', schoolId)
      .where('complete', '==', false).orderBy('classId', 'desc')).snapshotChanges().pipe(take(1)).toPromise();
  }

  getAllUnfinishedOrdersByClass(classId: string) {
    return this.afs.collection<Order>('orders', ref => ref.where('classId', '==', classId)
      .where('complete', '==', false)).snapshotChanges().pipe(take(1)).toPromise();
  }

  getAllUnfinishedOrdersByStudent(studentId: string) {
    return this.afs.collection<Order>('orders', ref => ref.where('studentId', '==', studentId)
      .where('complete', '==', false)).snapshotChanges().pipe(take(1)).toPromise();
  }

  getOrderById(orderId: string): Observable<Order> {
    return this.ordersCollection.doc<Order>(`${orderId}`).valueChanges();
  }

  updateOrder(orderToUpdate: Order): Promise<any> {
    return this.ordersCollection.doc(`${orderToUpdate.id}`).update(orderToUpdate);
  }

  async changeStudentIdInAllTheOrdersUnderStudent(oldStudentId: string, newStudentId: string) {
    // console.log('oldStudentId', oldStudentId);
    // console.log('newStudentId', newStudentId);
    
    const querySnapshot = await this.afs.collection<Order>('orders', ref => ref.where('studentId', '==', oldStudentId)).get().toPromise();
    querySnapshot.forEach(doc => {
      let copyOfAnOrder: Order = doc.data() as Order;
      copyOfAnOrder.id = doc.id;
      // console.log('order before', copyOfAnOrder);

      copyOfAnOrder.studentId = newStudentId;
      // console.log('order after', copyOfAnOrder);

      this.ordersCollection.doc(copyOfAnOrder.id).update(copyOfAnOrder);
    });
  }

  deleteOrder(orderToDelete: Order): Promise<any> {
    return this.ordersCollection.doc(`${orderToDelete.id}`).delete();
  }

  markOrderAsComplete(orderId: string) {
    return this.ordersCollection.doc(`${orderId}`).update({
      'complete': true
    });
  }
  // TODO

  getFirstOrdersUnderStudentManagerView(studentId: string, field: string, limit: number): Promise<DocumentChangeAction<Order>[]> {
    return this.afs.collection<Order>('orders', ref =>
      ref
        .where('studentId', '==', studentId)
        .orderBy(field, 'desc')
        .limit(limit)
    ).snapshotChanges().pipe(take(1)).toPromise();
  }


  getMore(studentId: string, field: string, limit: number, docTemp: QueryDocumentSnapshot<Order>): Promise<DocumentChangeAction<Order>[]> {
    // console.log(docTemp.payload.doc.id);
    
    return this.afs.collection<Order>('orders', ref =>
      ref
        .where('studentId', '==', studentId)
        .orderBy(field, 'desc')
        .limit(limit)
        .startAfter(docTemp)
    ).snapshotChanges().pipe(take(1)).toPromise();
  }
}
