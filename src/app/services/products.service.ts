import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, DocumentChangeAction } from 'angularfire2/firestore';
import { Product } from '../models/Product';
import { Observable } from 'rxjs';
import { PassToClassDialog } from '../models/PassToClassDialog';
import { take } from 'rxjs/operators';
import { Manager } from '../models/Manager';
import { ManagersService } from './managers.service';
import * as firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  productsCollections: AngularFirestoreCollection<Product>;

  constructor(private afs: AngularFirestore, private managersService: ManagersService) {
    this.productsCollections = this.afs.collection<Product>('products');
  }

  async addProduct(productToAdd: Product, managerInfo: Manager) {
    const manager = await this.managersService.getManagerById(managerInfo.id);
    const docId = manager.storeId + manager.productCounter;
    return this.productsCollections.doc(docId).set(productToAdd);
  }

  editProduct(productToUpdate: Product) {
    return this.productsCollections.doc(`${productToUpdate.id}`).update(productToUpdate);
  }
  getAllProductsUnderStore(storeId: string): Promise<DocumentChangeAction<Product>[]> {
    return this.afs.collection<Product>('products', ref => ref.where('storeId', '==', storeId))
      .snapshotChanges().pipe(take(1)).toPromise();
  }

  getAllProductsUnderStoreManager(storeId: string): Promise<DocumentChangeAction<Product>[]> {
    return this.afs.collection<Product>('products', ref => ref.where('storeId', '==', storeId))
      .snapshotChanges().pipe(take(1)).toPromise();
  }

  increaseProductCounter(productId: string) {
    this.productsCollections.doc(productId).update({ 'numberOfPurchase': firebase.firestore.FieldValue.increment(1) });
  }


  deleteProduct(productToDelete: Product) {
    return this.productsCollections.doc(`${productToDelete.id}`).delete();
  }
}
