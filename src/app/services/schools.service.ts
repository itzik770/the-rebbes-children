import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference, AngularFirestoreDocument, DocumentChangeAction } from 'angularfire2/firestore';
import { School } from '../models/School';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import { Campaign } from '../models/Campaign';
import { Class } from '../models/Class';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SchoolsService {

  schoolsCollections: AngularFirestoreCollection<School>;
  schools: Observable<School[]>;

  constructor(private afs: AngularFirestore) {
    this.schoolsCollections = this.afs.collection<School>('schools');
  }

  getSchools(): Promise<DocumentChangeAction<School>[]> {
    return this.afs.collection<School>('schools').snapshotChanges().pipe(take(1)).toPromise();
  }

  addClassToSchool(schoolId: string, newClass: Class): Promise<void> {
    return this.schoolsCollections.doc(schoolId).update({ 'classes': firebase.firestore.FieldValue.arrayUnion(newClass) });
  }

  removeClassFromSchool(schoolId: string, theClass: Class) {
    this.schoolsCollections.doc(schoolId).update({ 'classes': firebase.firestore.FieldValue.arrayRemove(theClass) });
  }

  addCampaignToSchool(schoolId: string, campaign: Campaign): Promise<void> {
    return this.schoolsCollections.doc(schoolId).update({ 'campaigns': firebase.firestore.FieldValue.arrayUnion(campaign) });
  }

  getSchoolById(schoolId: string): Observable<School> {
    return this.schoolsCollections.doc<School>(`${schoolId}`).valueChanges();
  }

  addSchool(school: School): Promise<DocumentReference> {
    return this.schoolsCollections.add(school);
  }

  updateSchool(schoolToUpdate: School): Promise<any> {
    return this.schoolsCollections.doc(schoolToUpdate.id).update(schoolToUpdate);
  }

  deleteSchool(SchoolToDelete: School): Promise<any> {
    return this.schoolsCollections.doc(SchoolToDelete.id).delete();
  }
}
