import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MatDialog } from '@angular/material/dialog';
import { WrongUserErrorComponent } from './components/error-msgs/wrong-user-error/wrong-user-error.component';

@Injectable({
  providedIn: 'root'
})
export class StudentAuthGuardGuard implements CanActivate {

  STORAGE_KEY = 'student_info';
  constructor(@Inject(LOCAL_STORAGE) public storage: StorageService,
    private router: Router,
    public dialog: MatDialog) { }
  canActivate(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const student = this.storage.get(this.STORAGE_KEY);
      // console.log('StudentAuthGuardGuard', student);

      if (student === undefined) {
        this.dialog.open(WrongUserErrorComponent).afterClosed().subscribe(() => {
          this.router.navigate(['/']);
          return resolve(false);
        });
      } else {
        return resolve(true);
      }
    });
  }

}
