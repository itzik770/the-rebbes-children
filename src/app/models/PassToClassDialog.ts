import { Manager } from './Manager';
import { Student } from './Student';

export interface PassToClassDialog {
    managerInfo: Manager;
    parentId: string;
}
