import { StudentList } from './StudentList';

// import { Student } from './Student';
// import { Campaign } from './Campaign';

export interface Class {
    id: string;
    name: string;
    studentList: Array<StudentList>// [StudentList];
    schoolId: string;
    scoreForAct: number;
    writingPermission: object;
    // classCode: string;
}
