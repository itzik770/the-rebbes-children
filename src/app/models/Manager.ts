export interface Manager {
    id: string;
    firebaseUid: string;
    schoolId: string;
    storeId: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    managerCode: string;
    classCounter: string;
    studentCounter: string;
    productCounter: number;
    campaignCounter: string;
    email: string;
    createDate: Date;
    writingPermission: {
        editor: string;
    };
}
