export interface Product {
    id: string;
    name: string;
    price: number;
    productPicture: string;
    storeId: string;
    numberOfPurchase: number;
    writingPermission: object;
}
