// import { Campaign } from './Campaign';
import { CampaignScore } from './CampaignScore';
import { Score } from './Score';
import { Order } from './Order';

export interface Student {
    id: string;
    firstName: string;
    lastName: string;
    classId: string;
    schoolId: string;
    campaignScores: CampaignScore[];
    pictureUrl: string;
    writingPermission: object;
    authorUid: string;
    recentScores: Array<Score>;
    recentOrders: Array<Order>;
    storeId: string;
    earnedPoints: number;
}
