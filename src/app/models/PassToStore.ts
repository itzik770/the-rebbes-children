export interface PassToStore {
    amountBefore: number;
    amountAfter: number;
}
