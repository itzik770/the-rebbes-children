import { Student } from './Student';

export interface StudentCampaignData {
    student: Student;
    campaignId: string;
    campaignName: string;
}
