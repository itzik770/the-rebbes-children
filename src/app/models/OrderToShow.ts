import { Order } from './Order';

export interface OrderToShow {
    className: string;
    orders: Order[];
}