export interface Campaign {
    id: string;
    schoolId: string;
    name: string;
    description: string;
    createDate: Date;
    expirationDate: Date;
    writingPermission: object;
}
