export interface Score {
    id: string;
    studentId: string;
    campaignId: string;
    scoreAmount: number;
    details: string;
    // complete: boolean;
    scoreDate: string;
    writingPermission: object;

}
