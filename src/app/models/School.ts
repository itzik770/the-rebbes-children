import { Campaign } from './Campaign';
import { Class } from './Class';

export interface School {
    id: string;
    name: string;
    city: string;
    classes: Class[];
    campaigns: Campaign[];
    writingPermission: {
        editor: string;
    };
}
