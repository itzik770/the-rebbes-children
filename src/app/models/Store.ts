import { Product } from './Product';

export interface Store {
    id: string;
    name: string;
    createDate: Date;
    expirationDate: Date;
    products: Product[];
    amountOfProducts: number;
    writingPermission: {
        editor: string;
    };
}
