export interface Order {
    id: string;
    studentId: string;
    studentName: string;
    classId: string;
    schoolId: string;
    campaignId: string;
    productId: string;
    details: string;
    complete: boolean;
    orderPicUrl: string;
    purchasePrice: number;
    purchaseDate: string;
    writingPermission: object;
}
