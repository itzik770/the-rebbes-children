export interface CampaignScore {
    campaignId: string;
    campaignName: string;
    earnedPoints: number;
    spandPoints: number;
    // writingPermission: object;
}
