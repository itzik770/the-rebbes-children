import { Injectable, Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserService } from './services/user.service';
import { ManagersService } from './services/managers.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { MatDialog } from '@angular/material/dialog';
import { WrongUserErrorComponent } from './components/error-msgs/wrong-user-error/wrong-user-error.component';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    public afAuth: AngularFireAuth,
    public userService: UserService,
    private managersService: ManagersService,
    public dialog: MatDialog,
    private router: Router) { }

  canActivate(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.userService.getCurrentUser()
        .then(user => {
          // this.router.navigate(['/manager_info']);
          return resolve(false);
        }, err => {
          console.log('error - canActivate', err);
          this.dialog.open(WrongUserErrorComponent).afterClosed().subscribe(() => {
            this.router.navigate(['/']);
            // return resolve(false);
            return resolve(true);
          });
        });
    });
  }
}
