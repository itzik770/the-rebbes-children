import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-not-enough-money',
  templateUrl: './not-enough-money.component.html',
  styleUrls: ['./not-enough-money.component.css']
})
export class NotEnoughMoneyComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public infoForBuying: { 'amountMissing': number }) { }

  ngOnInit() {
    // console.log('NotEnoughMoneyComponent', typeof (this.infoForBuying), this.infoForBuying);

  }
}
