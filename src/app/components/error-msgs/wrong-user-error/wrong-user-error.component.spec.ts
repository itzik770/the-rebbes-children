import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrongUserErrorComponent } from './wrong-user-error.component';

describe('WrongUserErrorComponent', () => {
  let component: WrongUserErrorComponent;
  let fixture: ComponentFixture<WrongUserErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrongUserErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrongUserErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
