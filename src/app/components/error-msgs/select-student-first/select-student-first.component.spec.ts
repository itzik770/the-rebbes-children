import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectStudentFirstComponent } from './select-student-first.component';

describe('SelectStudentFirstComponent', () => {
  let component: SelectStudentFirstComponent;
  let fixture: ComponentFixture<SelectStudentFirstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectStudentFirstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectStudentFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
