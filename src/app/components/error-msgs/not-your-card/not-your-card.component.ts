import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-your-card',
  templateUrl: './not-your-card.component.html',
  styleUrls: ['./not-your-card.component.css']
})
export class NotYourCardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
