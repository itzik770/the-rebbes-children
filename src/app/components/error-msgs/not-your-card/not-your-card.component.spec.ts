import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotYourCardComponent } from './not-your-card.component';

describe('NotYourCardComponent', () => {
  let component: NotYourCardComponent;
  let fixture: ComponentFixture<NotYourCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotYourCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotYourCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
