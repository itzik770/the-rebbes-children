import { Component, OnInit, Input, NgZone, ViewChild, ElementRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { StudentsService } from '../../services/students.service';
import { Observable, Subscription, timer } from 'rxjs';
import { Student } from '../../models/Student';
import { Router } from '@angular/router';
import { WrongUserErrorComponent } from '../error-msgs/wrong-user-error/wrong-user-error.component';
import { Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
// import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  showSpinner = false;
  @ViewChild('tempStudentId', { static: false }) tempStudentId: ElementRef;
  studentId = '';
  students: Observable<Student[]>;
  student: Student = {} as Student;
  STORAGE_KEY = 'student_info';
  studentInfo = {};
  interval = null;

  constructor(private studentService: StudentsService,
    public dialog: MatDialog,
    public ngZone: NgZone,
    public router: Router,
    @Inject(LOCAL_STORAGE) private storage: StorageService) {
  }

  ngOnInit() {
    this.storage.remove(this.STORAGE_KEY);
    this.studentInfo = this.storage.get(this.STORAGE_KEY) || {};
  }

  ngAfterViewInit() {
    this.focusInput();
  }

  ngOnDestroy() {
    this.clearFocusInterval();
  }

  clearFocusInterval() {
    // console.log('clearFocusInterval');
    
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  async focusInput() {
    this.interval = setInterval(() => {
      this.tempStudentId.nativeElement.focus();
    }, 1500)
  }

  async getStudentById() {
    try {
      this.showSpinner = true;
      this.clearFocusInterval();
      this.studentId = Number.parseInt(this.studentId).toString();
      const studentPromise = await this.studentService.getStudentById(this.studentId).toPromise();
      if (!studentPromise.payload.exists) {
        const dialogRef = this.dialog.open(WrongUserErrorComponent);
        dialogRef.afterClosed()
          .subscribe(() => {
            this.showSpinner = false;
            this.focusInput();
          });
      } else {
        this.student = studentPromise.payload.data();
        this.student.id = studentPromise.payload.id;
        this.studentInfo = Object.assign({}, this.student);
        this.storage.set(this.STORAGE_KEY, this.studentInfo);
        this.ngZone.run(() => {
          this.showSpinner = false;
          this.router.navigate(['/personal_area']);
        });
      }
    } catch (error) {
      console.log(error);
    }
  }

  // displayName() {
  //   console.log(this.tempStudentId, 'this.tempStudentId');

  //   this._timeout = null;
  //   if (this._timeout) { //if there is already a timeout in process cancel it
  //     window.clearTimeout(this._timeout);
  //   }
  //   this._timeout = window.setTimeout(() => {
  //     this._timeout = null;
  //     console.log(this.tempStudentId, 'this.tempStudentId');

  //     this.studentId = this.tempStudentId.slice(2);
  //     this.getStudentById();
  //   }, 1000);
  // }
}
