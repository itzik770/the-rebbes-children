import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { LoginComponent } from '../../login/login.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-main-navigation-bar',
  templateUrl: './main-navigation-bar.component.html',
  styleUrls: ['./main-navigation-bar.component.css']
})
export class MainNavigationBarComponent implements OnInit {
  isUserConnected = false;
  managerButtonText = '';
  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) { }
  ngOnInit() {
    this.isUserLoggedIn();
  }
  async isUserLoggedIn() {
    console.log('logon');
    
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.isUserConnected = true;
        this.managerButtonText = 'התנתק';
      } else {
        this.isUserConnected = false;
        this.managerButtonText = 'התחברות למנהלים';
      }
    });
  }

  connectOrDisconnect() {
    if (this.isUserConnected) {
      this.doLogout();
    } else {
      this.openLoginModal();
    }
  }

  openLoginModal() {
    let message = 'התחברת בהצלחה. הנך מועבר לעמוד המנהל';
    this.dialog.open(LoginComponent, {
      maxWidth: '100vw',
      maxHeight: '100vh',
      panelClass: 'login-modal'
    }).afterClosed().subscribe(async response => {
      if (!response) {
        message = 'ההתחברות נכשלה. נסה שנית מאוחר יותר';
      }
      this.snackBar.open(message, undefined, { duration: 3000, direction: 'rtl' });
    });
  }

  doLogout() {
    this.authService.doLogout();
  }

}
