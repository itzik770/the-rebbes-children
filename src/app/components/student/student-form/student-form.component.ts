import { Component, OnInit, Input, Output, EventEmitter, ContentChild, Inject } from '@angular/core';
import { Student } from 'src/app/models/Student';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CampaignScore } from 'src/app/models/CampaignScore';
import { OrdersService } from 'src/app/services/orders.service';
import { StudentsService } from 'src/app/services/students.service';
import { Score } from 'src/app/models/Score';
import { Order } from 'src/app/models/Order';
import { AddScoreComponent } from '../../score/add-score/add-score.component';
import { StudentCampaignData } from 'src/app/models/StudentCampaignData';
import { ClassesService } from 'src/app/services/classes.service';
import { Manager } from 'src/app/models/Manager';
import { StorageService, LOCAL_STORAGE } from 'ngx-webstorage-service';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  @Input() student: Student;
  @Input() classId: string;
  @Output() submitForm: EventEmitter<Student[]>;
  @Output() downloadUrl = new EventEmitter<string>();
  oldAndNewStudent: Student[] = [];
  oldStudentId: string;
  manager: Manager;
  STORAGE_KEY = 'manager_info';

  displayedScoreColumns: string[] = ['ניקוד', 'פרטים', 'תאריך'];
  displayedOrderColumns: string[] = ['מחיר', 'פרטים', 'האם קיבל', 'תאריך'];
  scoreDataSource;
  orderDataSource;
  @ContentChild(MatPaginator, { static: false }) scorePaginator: MatPaginator;
  @ContentChild(MatPaginator, { static: false }) orderPaginator: MatPaginator;
  studentCampaigns: CampaignScore[];
  studentScores: Score[];
  studentOrders: Order[];
  scoresPerCampaign: Score[];
  orderPerCampaign: Order[];
  studentForm: FormGroup;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;


  panelOpenState = true;
  campaignPanelOpenState = false;
  isCampaignSelected = false;
  selectedCampaignScore: CampaignScore;
  storeOrStudent: string;
  constructor(private fb: FormBuilder,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<StudentFormComponent>,
    private orderService: OrdersService,
    private classesService: ClassesService,
    private studentService: StudentsService,
    @Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    this.manager = this.storage.get(this.STORAGE_KEY) || {};
    // console.log(this.manager);


    this.firstFormGroup = this.fb.group({
      firstName: new FormControl(this.student.firstName || '', [Validators.required]),
      lastName: new FormControl(this.student.lastName || '', [Validators.required]),
      classId: new FormControl(this.student.classId || this.classId),
      schoolId: new FormControl(this.student.schoolId || this.manager.schoolId),
      campaignScores: new FormControl(this.student.campaignScores),
      authorUid: new FormControl(this.student.authorUid || this.manager.firebaseUid)
    });
    this.secondFormGroup = this.fb.group({
      pictureUrl: new FormControl(this.student.pictureUrl),
    });
    this.thirdFormGroup = this.fb.group({
      id: new FormControl(this.student.id || '', [Validators.required])
    })

    // this.studentForm = this.fb.group({
    //   firstName: new FormControl(this.student.firstName || '', [Validators.required]),
    //   lastName: new FormControl(this.student.lastName || '', [Validators.required]),
    //   id: new FormControl(this.student.id || '', [Validators.required]),
    //   classId: new FormControl(this.student.classId || this.classId),
    //   schoolId: new FormControl(this.student.schoolId || this.manager.schoolId),
    //   pictureUrl: new FormControl(this.student.pictureUrl),
    //   campaignScores: new FormControl(this.student.campaignScores)
    // });
    if (this.classId === undefined) {
      this.storeOrStudent = this.student.classId;
    } else {
      this.storeOrStudent = this.classId;
    }
    if (this.student.id) {
      const deepCopy = JSON.parse(JSON.stringify(this.student)) as Student;
      this.oldAndNewStudent.push(deepCopy);
      this.getAllStudentOrders();
    }
  }
  async getOrdersPerCampaign(campaignId: string) {
    return this.studentOrders.filter(order => order.campaignId === campaignId);
  }
  async getAllStudentOrders() {
    this.studentOrders = [];
    this.studentOrders = await this.orderService.getOrdersUnderStudent(this.student.id);
  }
  openAddScoreModal() {
    const studentAndCampaign: StudentCampaignData = {
      'student': this.student,
      'campaignId': this.selectedCampaignScore.campaignId,
      'campaignName': this.selectedCampaignScore.campaignName
    };
    this.dialog.open(AddScoreComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: studentAndCampaign
      }).afterClosed().subscribe(() => {
        this.getAllStudentOrders();
      });
  }

  async sortScoresByDate() {
    this.scoresPerCampaign.sort((a, b) => new Date(b.scoreDate).getTime() - new Date(a.scoreDate).getTime());
  }

  async sortOrdersByDate() {
    this.orderPerCampaign.sort((a, b) => new Date(b.purchaseDate).getTime() - new Date(a.purchaseDate).getTime());
  }

  async openStudentCampaignInfo(campaignScore: CampaignScore) {
    this.selectedCampaignScore = campaignScore;
  }

  onDownloadUrl(url: string) {
    this.secondFormGroup.value.pictureUrl = url;
    // this.studentForm.value.pictureUrl = url;
  }
  onSubmit() {
    // console.log(this.firstFormGroup);
    // console.log(this.secondFormGroup);
    // console.log('this.thirdFormGroup', this.thirdFormGroup);

    const updatedStudent = { ...this.student, ...this.firstFormGroup.value, ...this.secondFormGroup.value, ...this.thirdFormGroup.value } as Student;
    updatedStudent.id = Number.parseInt(updatedStudent.id).toString();
    this.oldAndNewStudent.push(updatedStudent);
    // this.oldAndNewStudent[0].id = this.oldStudentId;
    this.dialogRef.close();
    // console.log(this.oldStudentId, 'gsdfdgsd');

    this.submitForm.emit(this.oldAndNewStudent);
  }
  deleteStudent() {
    this.snackBar.open('מחיקה של תלמיד מוחקת את כל נתוניו! האם אתה בטוח?', 'אישור', { duration: 8000, direction: 'rtl' })
      .onAction()
      .subscribe(action => {
        this.dialogRef.close();
        this.studentService.deleteStudent(this.student.id);
        this.classesService.removeStudentFromClass(this.student);
      });
  }
}

