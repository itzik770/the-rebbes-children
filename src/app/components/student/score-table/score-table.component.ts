import { Component, OnInit, Input, ContentChild } from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { Score } from "src/app/models/Score";
import { ScoreService } from "src/app/services/score.service";
import { Student } from "src/app/models/Student";

@Component({
  selector: "app-score-table",
  templateUrl: "./score-table.component.html",
  styleUrls: ["./score-table.component.css"]
})
export class ScoreTableComponent implements OnInit {
  displayedColumns: string[] = ["scoreAmount", "details", "scoreDate"];
  dataSource: MatTableDataSource<Score>;
  studentCampaignScores: Score[] = [];
  @ContentChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ContentChild(MatSort, { static: false }) sort: MatSort;
  @Input() student: Student;
  constructor(private scoreService: ScoreService) {}

  async ngOnInit() {
    // this.getAllStudentCampaignScores(this.student.id);
    // console.log('this.student', this.student);

    this.dataSource = new MatTableDataSource(this.student.recentScores);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  // async organizeTableData() {}

  // async getAllStudentCampaignScores(studentId: string) {
  //   this.studentCampaignScores = [];
  //   this.studentCampaignScores = await this.scoreService.getFirstScoresUnderStudent(studentId);
  //   await this.organizeTableData();
  // }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
