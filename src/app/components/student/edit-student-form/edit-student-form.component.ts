import { Component, OnInit, Inject } from '@angular/core';
import { StudentsService } from 'src/app/services/students.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Student } from 'src/app/models/Student';
import { ClassesService } from 'src/app/services/classes.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Manager } from 'src/app/models/Manager';
import { ScoreService } from 'src/app/services/score.service';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-edit-student-form',
  templateUrl: './edit-student-form.component.html',
  styleUrls: ['./edit-student-form.component.css']
})
export class EditStudentFormComponent implements OnInit {
  STORAGE_KEY = 'manager_info';
  STUDENT_KEY = 'student_info';

  manager: Manager;
  constructor(
    private snackBar: MatSnackBar,
    private classesService: ClassesService,
    private studentsService: StudentsService,
    private dialogRef: MatDialogRef<EditStudentFormComponent>,
    @Inject(MAT_DIALOG_DATA) public studentToUpdate: Student,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private scoresService: ScoreService,
    private ordersService: OrdersService) { }

  ngOnInit() {
    this.manager = this.storage.get(this.STORAGE_KEY) || {};
  }

  async syncStudentInfo(studentId: string) {
    const studentData = await this.studentsService.getStudentById(studentId).toPromise();
    const student: Student = studentData.payload.data();
    student.id = studentData.payload.id;
    this.storage.set(this.STUDENT_KEY, student);
  }

  async updateStudent(oldAndNewStudentInfo: Student[]) {
    let studentId = oldAndNewStudentInfo[0].id
    if (oldAndNewStudentInfo[0].id !== oldAndNewStudentInfo[1].id) {
      const oldStudentId = oldAndNewStudentInfo[0].id;
      const newStudentId = oldAndNewStudentInfo[1].id;
      studentId = newStudentId;
      console.log('Change ID!!!');
      try {
        await this.ordersService.changeStudentIdInAllTheOrdersUnderStudent(oldStudentId, newStudentId);
        await this.scoresService.changeStudentIdInAllTheScoresUnderStudent(oldStudentId, newStudentId);
        await this.studentsService.addStudent(oldAndNewStudentInfo[1]);
        await this.studentsService.deleteStudent(oldStudentId);
        await this.classesService.changeStudentNameInClassList(oldAndNewStudentInfo[0], oldAndNewStudentInfo[1]);
      } catch (error) {
        console.log(error);
        
      }
    } else if (`${oldAndNewStudentInfo[0].firstName} ${oldAndNewStudentInfo[0].lastName}` !== `${oldAndNewStudentInfo[1].firstName} ${oldAndNewStudentInfo[1].lastName}`) {
      console.log('new NAME entered');
      await this.classesService.changeStudentNameInClassList(oldAndNewStudentInfo[0], oldAndNewStudentInfo[1]);
      this.studentsService.updateStudent(oldAndNewStudentInfo[1]).then(() => {
        this.dialogRef.beforeClosed().subscribe(() => {
          this.snackBar.open('עידכון התלמיד הסתיים בהצלחה', undefined, { duration: 3000, direction: 'rtl' });
        });
      }).catch(error => {
        this.snackBar.open('עידכון התלמיד נכשל', undefined, { duration: 3000, direction: 'rtl' });
      });
    } else {
      this.studentsService.updateStudent(oldAndNewStudentInfo[1]).then(() => {
        this.dialogRef.beforeClosed().subscribe(() => {
          this.snackBar.open('עידכון התלמיד הסתיים בהצלחה', undefined, { duration: 3000, direction: 'rtl' });
        });
      }).catch(error => {
        this.snackBar.open('עידכון התלמיד נכשל', undefined, { duration: 3000, direction: 'rtl' });
      });
    }
    this.storage.set(this.STUDENT_KEY, oldAndNewStudentInfo[1]);
  }
}
