import { Component, OnInit, Inject } from '@angular/core';
import { Student } from '../../../models/Student';
import { Order } from 'src/app/models/Order';
import { Score } from 'src/app/models/Score';
import { Router } from '@angular/router';
import { StorageService, LOCAL_STORAGE } from 'ngx-webstorage-service';
import { StudentsService } from 'src/app/services/students.service';

@Component({
  selector: 'app-student-info',
  templateUrl: './student-info.component.html',
  styleUrls: ['./student-info.component.css']
})
export class StudentInfoComponent implements OnInit {

  showStudent = false;
  studentOrders: Order[];
  studentScores: Score[];
  STORAGE_KEY = 'student_info';
  student: Student = {} as Student;

  constructor(@Inject(LOCAL_STORAGE) public storage: StorageService,
    private studentsService: StudentsService,
    private router: Router) { }

  ngOnInit() {
    this.fetchStudent();
  }
  async fetchStudent() {
    // this.student 
    const tempStudent: Student = await this.storage.get(this.STORAGE_KEY);
    const tempStudentData = await this.studentsService.getStudentById(tempStudent.id).toPromise();
    // : Student
    this.student = tempStudentData.payload.data();
    this.student.id = tempStudentData.payload.id;
    this.storage.set(this.STORAGE_KEY, this.student);
    // this.student = this.storage.
      if(Object.entries(this.student).length === 0 && this.student.constructor === Object) {
      this.showStudent = false;
    } else {
      this.showStudent = true;
    }
  }

  async goToStore() {
    this.router.navigate(['store']);
  }

  async exitAccount() {
    this.storage.remove(this.STORAGE_KEY);
    this.router.navigate(['/']);
  }
}
