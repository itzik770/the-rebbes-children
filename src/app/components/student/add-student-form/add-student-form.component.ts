import { Component, OnInit, Inject } from '@angular/core';
import { Student } from 'src/app/models/Student';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PassToClassDialog } from 'src/app/models/PassToClassDialog';
import { StudentsService } from 'src/app/services/students.service';
import { ManagersService } from 'src/app/services/managers.service';
import { ClassesService } from 'src/app/services/classes.service';
import { CampaignsService } from 'src/app/services/campaigns.service';
import { Campaign } from 'src/app/models/Campaign';
import { CampaignScore } from 'src/app/models/CampaignScore';

@Component({
  selector: 'app-add-student-form',
  templateUrl: './add-student-form.component.html',
  styleUrls: ['./add-student-form.component.css']
})

export class AddStudentFormComponent implements OnInit {
  classId = '';
  student = {} as Student;
  newStudentId = '';
  // isStudentExists = false;
  allStudents: Student[] = [];
  schoolsCampaigns: Campaign[] = [];
  studentCampaigns: CampaignScore[] = [];
  constructor(private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<AddStudentFormComponent>,
    private managersService: ManagersService,
    private classesService: ClassesService,
    private studentsService: StudentsService,
    private campaignsService: CampaignsService,
    @Inject(MAT_DIALOG_DATA) public importantInfo: PassToClassDialog) { }

  ngOnInit() {
    // console.log(this.student);

    this.getStudentUnderClassId();
    this.getAllSchoolCampaigns();
    this.classId = this.importantInfo.parentId;
  }

  getStudentUnderClassId() {
    // console.log('this.importantInfo.parentId', this.importantInfo.parentId);

    this.studentsService.getStudentsUnderClass(this.importantInfo.parentId)
      .subscribe(studentsData => {
        this.allStudents = [];
        studentsData.map(studentData => {
          const data = studentData.payload.doc.data();
          const id = studentData.payload.doc.id;
          this.allStudents.push({ id, ...data });
        });
      });
  }

  getAllSchoolCampaigns() {
    this.campaignsService.getCampaignsUnderSchoolId(this.importantInfo.managerInfo.schoolId)
      .subscribe(campaignsData => {
        campaignsData.map(campaignData => {
          const data = campaignData.payload.doc.data();
          const id = campaignData.payload.doc.id;
          this.studentCampaigns.push({ campaignId: id, campaignName: data.name, earnedPoints: 0, spandPoints: 0 });
          this.schoolsCampaigns.push({ id, ...data });
        });
      });
  }

  async organizeStudentData() {
    this.student.storeId = this.importantInfo.managerInfo.storeId;
    this.student.campaignScores = this.studentCampaigns;
    this.student.classId = this.importantInfo.parentId;
    this.student.firstName = this.student.firstName.trim();
    this.student.lastName = this.student.lastName.trim();
    this.student.earnedPoints = 0;
  }
  async addStudent(studentToAdd: Student[]) {
    this.student = studentToAdd[0];
    let snackBarMessage: string;
    try {
      const studentsNames = [];
      for (const studentData of this.allStudents) {
        studentsNames.push(`${studentData.firstName.trim()} ${studentData.lastName.trim()}`);
      }
      const studentFullName = `${this.student.firstName.trim()} ${this.student.lastName.trim()}`;
      if (studentsNames.includes(studentFullName)) {
        snackBarMessage = `התלמיד ${studentFullName} כבר קיים בכיתה!`;
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      } else {
        await this.organizeStudentData();
        // this.managersService.
        // const studentRef = 
        await this.studentsService.addStudent(this.student);
        // console.log('AddStudent', this.student);
        
        // await this.studentsService.addStudent(this.student, this.importantInfo);
        // console.log('studentRef', studentRef);
        // this.newStudentId = studentRef;
        // this.classesService.addStudentToClass(studentRef, this.importantInfo.parentId);
        this.classesService.addStudentToClassWithName(this.student);
        
        // אני לא חושב שצריך להגדיל את מספר התלמידים היות וזה לא ע"פ מספר מסודר אלא רנדומלי
        // this.managersService.increaseStudentCounter(this.importantInfo.managerInfo.id);
        
        // this.student.id = studentRef;
        snackBarMessage = `התלמיד ${studentFullName} נוסף בהצלחה!`;
        this.snackBar.open(snackBarMessage, undefined, { duration: 5000, direction: 'rtl' });
        this.dialogRef.close();
      }

      // this.dialogRef.beforeClosed().subscribe(() => {
      //   this.snackBar.open(snackBarMessage, undefined, { duration: 5000, direction: 'rtl' });
      // });
    } catch (error) {
      console.log(error);
      if (error === 'student id exists') {
        this.snackBar.open('המספר הסידורי שהכנסת קיים כבר במערכת החלף כרטיס ונסה שוב', undefined, { duration: 6000, direction: 'rtl' });
      }
      this.snackBar.open('הוספת התלמיד נכשלה', undefined, { duration: 3000, direction: 'rtl' });

    }
  }
}
