import { Component, OnInit, Input, ContentChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Order } from 'src/app/models/Order';
import { Student } from 'src/app/models/Student';
import { OrdersService } from 'src/app/services/orders.service';
import { ProductsService } from 'src/app/services/products.service';
import { Product } from 'src/app/models/Product';

@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})
export class OrderTableComponent implements OnInit {
  displayedColumns: string[] = ['complete', 'productPicUrl', 'details', 'purchasePrice', 'purchaseDate'];
  dataSource: MatTableDataSource<Order>;
  studentCampaignOrders: Order[] = [];
  products: Product[] = [];
  @ContentChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ContentChild(MatSort, {static: false}) sort: MatSort;
  @Input() student: Student;
  constructor(private orderService: OrdersService,
    private productService: ProductsService) { }

  async ngOnInit() {
    // this.getAllStudentCampaignOrders(this.student.id);
    this.organizeTableData();
  }
  async organizeTableData() {
    this.dataSource = new MatTableDataSource(this.student.recentOrders);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // async getAllStudentCampaignOrders(studentId: string) {
  //   this.studentCampaignOrders = await this.orderService.getFirstOrdersUnderStudent(studentId);
  //   await this.getAllProducts();
  //   await this.organizeTableData();
  // }

  // async getAllProducts() {
  //   const productsData = await this.productService.getAllProductsUnderStore(this.student.storeId);
  //   productsData.map(async productData => {
  //     const data = productData.payload.doc.data();
  //     const id = productData.payload.doc.id;
  //     this.products.push({ id, ...data });
  //   });
  // }
  // async getAllOrdersPictures() {
  //   this.products.map(async product => {
  //     this.studentCampaignOrders.map(order => {
  //       if (order.productId === product.id) {
  //         order.productId = product.productPicture;
  //       }
  //     });
  //   });
  // }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
