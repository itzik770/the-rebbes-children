import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { StudentsService } from 'src/app/services/students.service';
import { Manager } from 'src/app/models/Manager';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  @Input() managerInfo: Manager;
  @Input() storeOrStudent: string;
  @Output() downloadUrl = new EventEmitter<string>();
  task: AngularFireUploadTask;
  percentage: Observable<number>;
  snapshot: Observable<any>;
  downloadURL: string;
  isHovering: boolean;
  constructor(private storage: AngularFireStorage) { }

  ngOnInit() {
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
    // console.log('toggleHover', event);
    
  }

  startUpload(event: FileList) {
    // console.log('startUpload', event);

    const file = event.item(0);
    if (file.type.split('/')[0] !== 'image') {
      console.error('unsupported file type :( ');
      return;
    }
    const path = `/${this.managerInfo.schoolId}/${this.storeOrStudent}/${this.managerInfo.studentCounter}_${new Date().getTime()}`;
    const ref = this.storage.ref(path);
    const customMetadata = { app: 'RebbesChildren', schoolId: this.managerInfo.schoolId };
    try {
      this.task = this.storage.upload(path, file, { customMetadata });
      this.percentage = this.task.percentageChanges();
      this.snapshot = this.task.snapshotChanges().pipe(
        finalize(async () => {
          this.downloadURL = await ref.getDownloadURL().toPromise();
          this.downloadUrl.emit(this.downloadURL);
        }),
      );
    } catch (error) {
      console.log(error);
      
    }
  }

  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes;
  }
}
