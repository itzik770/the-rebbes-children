import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClassesService } from 'src/app/services/classes.service';
import { Class } from 'src/app/models/Class';
import { StudentsService } from 'src/app/services/students.service';
import { SchoolsService } from 'src/app/services/schools.service';

@Component({
  selector: 'app-edit-class-form',
  templateUrl: './edit-class-form.component.html',
  styleUrls: ['./edit-class-form.component.css']
})
export class EditClassFormComponent implements OnInit {
  constructor(
    private snackBar: MatSnackBar,
    private schoolsService: SchoolsService,
    private classesService: ClassesService,
    private studentsService: StudentsService,
    @Inject(MAT_DIALOG_DATA) public classToUpdate: Class,
    private dialogRef: MatDialogRef<EditClassFormComponent>) { }

  ngOnInit() {
  }

  updateClass(aClass: Class) {
    this.classesService.updateClass(aClass).then(() => {
      this.dialogRef.beforeClosed().subscribe(() => {
        this.snackBar.open('עידכון הכיתה הסתיים בהצלחה', undefined, { duration: 3000, direction: 'rtl' });
      });
    }).catch(error => {
      this.snackBar.open('עידכון הכיתה נכשל', undefined, { duration: 3000, direction: 'rtl' });
    });
  }

  deleteClass() {
    this.snackBar.open('מחיקה של כיתה מוחקת גם את כל תלמידיה! האם אתה בטוח?', 'אישור', { duration: 8000, direction: 'rtl' })
      .onAction()
      .subscribe(action => {
        this.studentsService.deleteStudentUnderClass(this.classToUpdate.id);
        this.classesService.deleteClass(this.classToUpdate.id);
        // console.log(this.classToUpdate);
        
        this.schoolsService.removeClassFromSchool(this.classToUpdate.schoolId, this.classToUpdate)
        this.dialogRef.close();
      });
  }
}
