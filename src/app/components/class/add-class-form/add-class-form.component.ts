import { Component, OnInit, Inject } from '@angular/core';
import { Class } from 'src/app/models/Class';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClassesService } from 'src/app/services/classes.service';
import { SchoolsService } from 'src/app/services/schools.service';
import { Manager } from 'src/app/models/Manager';
import { ManagersService } from 'src/app/services/managers.service';
import { StudentList } from 'src/app/models/StudentList';

@Component({
  selector: 'app-add-class-form',
  templateUrl: './add-class-form.component.html',
  styleUrls: ['./add-class-form.component.css']
})
export class
  AddClassFormComponent implements OnInit {
  newClass: Class = {} as Class;
  manager: Manager;
  allClasses: Class[] = [];
  constructor(private snackBar: MatSnackBar,
    private managersService: ManagersService,
    private schoolsService: SchoolsService,
    private classesService: ClassesService,
    private dialogRef: MatDialogRef<AddClassFormComponent>,
    @Inject(MAT_DIALOG_DATA) public managerInfo: Manager) { }

  ngOnInit() {
    this.getClassesUnderSchoolId();
  }

  async getClassesUnderSchoolId() {
    const classesData = await this.classesService.getClassesBySchoolId(this.managerInfo.schoolId);
    classesData.map(classData => {
      const data = classData.payload.doc.data();
      const id = classData.payload.doc.id;
      this.allClasses.push({ id, ...data });
    });
  }

  async addClass(aClass: Class) {
    let snackBarMessage: string;
    try {
      const classesNames = [];
      for (const classData of this.allClasses) {
        await classesNames.push(classData.name);
      }
      if (classesNames.includes(aClass.name)) {
        snackBarMessage = `כיתה ${aClass.name} כבר קיימת!`;
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      } else {
        this.newClass = {
          name: `${aClass.name}`,
          schoolId: this.managerInfo.schoolId,
          studentList: [],
          scoreForAct: aClass.scoreForAct
        } as Class;
        if (aClass.name === '') {
          throw Error;;
        }
        const classRef = await this.classesService.addClass(this.managerInfo, this.newClass);
        this.newClass.id = classRef;
        this.schoolsService.addClassToSchool(this.managerInfo.schoolId, this.newClass);
        this.managersService.increaseClassCounter(this.managerInfo.id);
        snackBarMessage = `כיתה ${aClass.name} נוספה בהצלחה!`;
        // this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      }
      this.dialogRef.beforeClosed().subscribe(() => {
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      });
    } catch (error) {
      this.snackBar.open('הוספת הכיתה נכשלה', undefined, { duration: 3000, direction: 'rtl' });
      console.log('error', error);
      
    }
    this.dialogRef.close('this.newClass');
  }
}
