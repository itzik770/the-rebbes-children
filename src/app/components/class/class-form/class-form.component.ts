import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Class } from 'src/app/models/Class';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-class-form',
  templateUrl: './class-form.component.html',
  styleUrls: ['./class-form.component.css']
})
export class ClassFormComponent implements OnInit {

  @Input() class: Class;
  @Output() submitForm: EventEmitter<Class>;
  classForm: FormGroup;

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<ClassFormComponent>
    ) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    // console.log(this.class);
    
    this.classForm = this.fb.group({
      name: new FormControl(this.class.name, [Validators.required]),
      studentList: new FormControl(this.class.studentList || []),
      schoolId: new FormControl(this.class.schoolId),
      scoreForAct: new FormControl(this.class.scoreForAct)
    });
  }

  onSubmit() {
    this.class = { ...this.class, ...this.classForm.value};
    this.dialogRef.close();
    this.submitForm.emit(this.class);
  }
}
