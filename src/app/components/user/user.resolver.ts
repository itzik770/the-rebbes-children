import { Injectable, Inject } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { FirebaseUserModel } from 'src/app/models/User';
import { UserService } from 'src/app/services/user.service';
import { ManagersService } from 'src/app/services/managers.service';
import { Manager } from 'src/app/models/Manager';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable()
export class UserResolver implements Resolve<FirebaseUserModel> {
    STORAGE_KEY = 'manager_info';
    constructor(private userService: UserService,
        private router: Router,
        private managerService: ManagersService,
        @Inject(LOCAL_STORAGE) public storage: StorageService) { }

    async resolve(route: ActivatedRouteSnapshot): Promise<FirebaseUserModel> {
        return new Promise(async (resolve, reject) => {
            try {
                const result = await this.userService.getCurrentUser();
                if (result.providerData[0].providerId === 'password') {
                    const managerPromise = await this.managerService.getManagerByUid(result.uid);
                    console.log(result);
                    console.log(managerPromise);
                    
                    const manager = managerPromise[0].payload.doc.data() as Manager;
                    manager.id = managerPromise[0].payload.doc.id;
                    if (this.storage.get('king')) {
                        return resolve();
                    } else if (manager.id) {
                        if (manager.firstName === 'איציק' && manager.lastName === 'פורטי') {
                            this.storage.set('king', true);
                        }
                        this.storage.set(this.STORAGE_KEY, manager);
                        return resolve();
                    } else {
                        this.router.navigate(['/']);
                        alert('לא קיים מנהל כזה');
                    }
                } else {
                    this.router.navigate(['/']);
                    alert('לא קיים מנהל כזה');
                }
            } catch (error) {
                console.log(error);
            }
        });
    }
}
