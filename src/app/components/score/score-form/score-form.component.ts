import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Score } from 'src/app/models/Score';
import { ScoreService } from 'src/app/services/score.service';
import { StudentsService } from 'src/app/services/students.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StudentCampaignData } from 'src/app/models/StudentCampaignData';

@Component({
  selector: 'app-score-form',
  templateUrl: './score-form.component.html',
  styleUrls: ['./score-form.component.css']
})
export class ScoreFormComponent implements OnInit {
  @Input() score: Score;
  @Input() passedStudentAndCampaign: StudentCampaignData;
  @Output() submitForm: EventEmitter<Score>;
  scoreForm: FormGroup;
  constructor(
    private scoreService: ScoreService,
    private studentService: StudentsService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ScoreFormComponent>,
    @Inject(MAT_DIALOG_DATA) public studentCampaignData: StudentCampaignData
  ) {
    this.submitForm = new EventEmitter();
  }

  ngOnInit() {
    this.scoreForm = this.fb.group({
      studentId: new FormControl(this.passedStudentAndCampaign.student.id || ''),
      campaignId: new FormControl(this.studentCampaignData.campaignId),
      campaignName: new FormControl(this.studentCampaignData.campaignName),
      scoreAmount: new FormControl(),
      details: new FormControl(''),
      complete: new FormControl(false),
      scoreDate: new FormControl(new Date().toISOString())
    });
  }

  onSubmit() {
    if (this.scoreForm.value.studentId === '') {
      this.scoreForm.value.studentId = this.passedStudentAndCampaign.student.id;
    }
    this.score = this.scoreForm.value;
    this.scoreService.addScore(this.score);
    // TODO
    this.studentService.addStudentScore(this.studentCampaignData.student.id, this.studentCampaignData.campaignId, this.score.scoreAmount);
    this.submitForm.emit(this.score);
    this.dialogRef.close();
  }
}
