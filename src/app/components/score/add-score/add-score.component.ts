import { Component, OnInit, Inject } from '@angular/core';
import { Score } from 'src/app/models/Score';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StudentCampaignData } from 'src/app/models/StudentCampaignData';

@Component({
  selector: 'app-add-score',
  templateUrl: './add-score.component.html',
  styleUrls: ['./add-score.component.css']
})
export class AddScoreComponent implements OnInit {

  newScore = {} as Score;
  constructor(@Inject(MAT_DIALOG_DATA) public studentAndCampaign: StudentCampaignData) { }

  ngOnInit() {
  }

  addScore(theNewScore: Score) {
    this.newScore = theNewScore;
  }
}
