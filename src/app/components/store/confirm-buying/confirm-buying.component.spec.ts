import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmBuyingComponent } from './confirm-buying.component';

describe('ConfirmBuyingComponent', () => {
  let component: ConfirmBuyingComponent;
  let fixture: ComponentFixture<ConfirmBuyingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmBuyingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmBuyingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
