import { Component, OnInit, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Student } from 'src/app/models/Student';
import { StudentsService } from 'src/app/services/students.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-buying',
  templateUrl: './confirm-buying.component.html',
  styleUrls: ['./confirm-buying.component.css']
})
export class ConfirmBuyingComponent implements OnInit {
  student: Student = {} as Student;
  studentId = '';
  STORAGE_KEY = 'student_info';
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService,
    private dialogRef: MatDialogRef<ConfirmBuyingComponent>) { }

  ngOnInit() {
    this.fetchStudent();
  }

  async fetchStudent() {
    this.student = await this.storage.get(this.STORAGE_KEY);
  }
  async getStudentById() {
    try {
      this.studentId = Number.parseInt(this.studentId).toString();
      // console.log(this.studentId, 'this.studentId');
      this.dialogRef.close(this.student.id === this.studentId);
    } catch (error) {
      console.log(error);
    }
  }
}
