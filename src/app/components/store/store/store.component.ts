import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Manager } from 'src/app/models/Manager';
import { Product } from 'src/app/models/Product';
import { ProductsService } from 'src/app/services/products.service';
import { Student } from 'src/app/models/Student';
import { StorageService, LOCAL_STORAGE } from 'ngx-webstorage-service';
import { ConfirmBuyingComponent } from '../confirm-buying/confirm-buying.component';
import { NotEnoughMoneyComponent } from '../../error-msgs/not-enough-money/not-enough-money.component';
import { SuccessfullyBoughtComponent } from '../../success-msgs/successfully-bought/successfully-bought.component';
import { NotYourCardComponent } from '../../error-msgs/not-your-card/not-your-card.component';
import { StudentsService } from 'src/app/services/students.service';
import { OrdersService } from 'src/app/services/orders.service';
import { Order } from 'src/app/models/Order';
import { StoresService } from 'src/app/services/stores.service';
import { Store } from 'src/app/models/Store';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {
  showSpinner = true;
  manager: Manager;
  products: Product[] = [];
  orders: Order[] = [];
  unfinishedOrders: Order[] = [];
  numberOfProducts: number;
  STORAGE_KEY = 'student_info';
  student: Student = {} as Student;
  store: Store = {} as Store;
  constructor(private dialog: MatDialog,
    @Inject(LOCAL_STORAGE) public storage: StorageService,
    private studentsService: StudentsService,
    private storeService: StoresService,
    private productService: ProductsService,
    private ordersService: OrdersService) { }


  ngOnInit() {
    this.fetchStudent();
  }

  fetchStudent() {
    this.student = this.storage.get(this.STORAGE_KEY);
    this.fetchRecentData(this.student.id);
    if (Object.entries(this.student).length === 0 && this.student.constructor === Object) {
      // TODO
    } else {
      console.log('pip');
      
      this.getStoreData(this.student.storeId);
    }
  }

  async fetchRecentData(studentId: string) {
    const studentData = await this.studentsService.getStudentById(studentId).toPromise();
    this.student = studentData.payload.data();
  }

  async getUpdatedStudentInfo() {
    const studentData = await this.studentsService.getStudentById(this.student.id).toPromise();
    this.student = studentData.payload.data();
    this.student.id = studentData.payload.id;
  }
  async getStoreData(storeId: string) {
    const storeData = await this.storeService.getStoreById(storeId);
    this.store = storeData.payload.data()
    this.store.id = storeData.payload.id;
    this.products = this.store.products;
    this.numberOfProducts = this.products.length;
    this.showSpinner = false;
  }

  createOrder(order: Product) {
    const orderToAdd = {} as Order;
    orderToAdd.campaignId = this.student.campaignScores[0].campaignId;
    orderToAdd.classId = this.student.classId;
    orderToAdd.complete = false;
    orderToAdd.details = order.name;
    orderToAdd.productId = order.id;
    orderToAdd.orderPicUrl = order.productPicture;
    orderToAdd.purchaseDate = new Date().toISOString();
    orderToAdd.purchasePrice = order.price;
    orderToAdd.schoolId = this.student.schoolId;
    orderToAdd.studentId = this.student.id;
    orderToAdd.studentName = `${this.student.firstName} ${this.student.lastName}`;
    return orderToAdd;
  }

  isAffordable(productPrice: number) {
    return this.student.campaignScores[0].earnedPoints - this.student.campaignScores[0].spandPoints >= productPrice;
  }

  async tryToBuy(passedProduct: Product) {
    // this.showSpinner = true;
    // console.log(this.showSpinner);
    const amountBeforeBuying = this.student.campaignScores[0].earnedPoints - this.student.campaignScores[0].spandPoints;
    if (this.isAffordable(passedProduct.price)) {

      this.dialog.open(ConfirmBuyingComponent, {
        maxWidth: '80vw',
        maxHeight: '80vh'
      })
        .beforeClosed().subscribe(async response => {
          this.showSpinner = true;
          if (response) {
            try {
              await this.studentsService.increaseStudentSpandAmount(this.student.id,
                this.student.campaignScores[0].campaignId,
                passedProduct.price);
              const orderToAdd = this.createOrder(passedProduct);
              await this.ordersService.addOrder(orderToAdd);
              this.productService.increaseProductCounter(passedProduct.id);
              this.storeService.increasePurchasesCounter(this.student.storeId);
              if (this.student.recentOrders) {
                this.student.recentOrders.unshift(orderToAdd);
                this.student.recentOrders.pop();
              }
              setTimeout(async () => {
                await this.getUpdatedStudentInfo();
                this.storage.set(this.STORAGE_KEY, this.student);
              }, 1500);
              const dialogRef = this.dialog.open(SuccessfullyBoughtComponent, {
                data: {
                  'amountBefore': amountBeforeBuying, 'amountAfter': amountBeforeBuying - passedProduct.price
                }
              });
              dialogRef.disableClose = true;
              this.showSpinner = false;
              
              setTimeout(() => {
                dialogRef.close();
              }, 2000);
              
            } catch (error) {
              console.log(error);
            }
          } else {
            this.dialog.open(NotYourCardComponent)
              .beforeClosed()
              .subscribe(() => {
                this.showSpinner = false;
              });
          }
        });
    } else {
      this.dialog.open(NotEnoughMoneyComponent, {
        data: {
          'amountMissing': passedProduct.price - amountBeforeBuying
        }
      })
        .beforeClosed()
        .subscribe(() => {
          this.showSpinner = false;
        });
    }
  }
}
