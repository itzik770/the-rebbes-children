import { Component, OnInit, Inject } from "@angular/core";
import { SchoolsService } from "src/app/services/schools.service";
import { School } from "src/app/models/School";
import { Router } from "@angular/router";
import { ManagersService } from "src/app/services/managers.service";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
import { Manager } from "src/app/models/Manager";
import { MatDialog } from "@angular/material/dialog";
import { AddSchoolFormComponent } from "../school/add-school-form/add-school-form.component";
import { EditSchoolFormComponent } from "../school/edit-school-form/edit-school-form.component";
import { AddManagerFormComponent } from "../manager/add-manager-form/add-manager-form.component";
import { EditManagerFormComponent } from "../manager/edit-manager-form/edit-manager-form.component";
import { AddStoreFormComponent } from "../store/add-store-form/add-store-form.component";
import { StudentsService } from "src/app/services/students.service";
import { Student } from "src/app/models/Student";

@Component({
  selector: "app-admin",
  templateUrl: "./admin.component.html",
  styleUrls: ["./admin.component.css"]
})
export class AdminComponent implements OnInit {
  manager_key = "manager_info";
  showSpinner = true;
  allSchools: School[] = [];
  allTheManagers: Manager[] = [];
  allStudents: Student[] = [];

  constructor(
    private schoolsService: SchoolsService,
    private router: Router,
    private managerService: ManagersService,
    private studentsService: StudentsService,
    private dialog: MatDialog,
    @Inject(LOCAL_STORAGE) public storage: StorageService
  ) {}

  ngOnInit() {
    this.getSchoolsData();
    this.getManagersData();
  }

  async getSchoolsData() {
    this.showSpinner = false;
    const schoolsData = await this.schoolsService.getSchools();
    schoolsData.map(school => {
      const data = school.payload.doc.data();
      const id = school.payload.doc.id;
      this.allSchools.push({ id, ...data });
    });
  }

  async getManagersData() {
    this.showSpinner = false;
    const managersData = await this.managerService.getManagers();
    managersData.map(manager => {
      const data = manager.payload.doc.data();
      const id = manager.payload.doc.id;
      this.allTheManagers.push({ id, ...data });
    });
  }

  async chooseSchoolModal(passedSchool: School) {
    const managerPromise = await this.managerService.getManagerByUid(
      passedSchool.writingPermission.editor
    );
    const manager = managerPromise[0].payload.doc.data() as Manager;
    manager.id = managerPromise[0].payload.doc.id;
    this.storage.set(this.manager_key, manager);
    this.router.navigate(["/manager_info"]);
  }

  openAddSchoolModal() {
    this.dialog.open(AddSchoolFormComponent);
  }

  openSchoolModalToEdit(school: School) {
    this.dialog.open(EditSchoolFormComponent, {
      data: school
    });
  }

  openAddManagerModal() {
    this.dialog.open(AddManagerFormComponent);
  }

  openManagerModalToEdit(manager: Manager) {
    this.dialog.open(EditManagerFormComponent, {
      data: manager
    });
  }

  async fixAllStudents() {
    const studentsData = await this.studentsService.getStudents();
    studentsData.map(studentData => {
      const id = studentData.payload.doc.id;
      const data = studentData.payload.doc.data();
      this.allStudents.push({ id, ...data });
    });
    // console.log(this.allStudents);
    this.allStudents.map(student => {
      this.studentsService.addPointFieldUnderStudent(student);
    })
  }

  openAddStoreModal() {
    this.dialog.open(AddStoreFormComponent);
  }
}
