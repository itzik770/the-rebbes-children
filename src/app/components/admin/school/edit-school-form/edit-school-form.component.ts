import { Component, OnInit, Inject } from "@angular/core";
import { School } from "src/app/models/School";
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from "@angular/material";
import { SchoolsService } from 'src/app/services/schools.service';

@Component({
  selector: "app-edit-school-form",
  templateUrl: "./edit-school-form.component.html",
  styleUrls: ["./edit-school-form.component.css"]
})
export class EditSchoolFormComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public schoolToUpdate: School,
    private snackBar: MatSnackBar,
    private schoolsService: SchoolsService,
    private dialogRef: MatDialogRef<EditSchoolFormComponent>) {}

  ngOnInit() {}

  updateSchool(aSchool: School) {
    this.schoolsService.updateSchool(aSchool)
      .then(() => {
        this.dialogRef.beforeClosed().subscribe(() => {
          this.snackBar.open("עידכון בית הספר הסתיים בהצלחה", undefined, {
            duration: 3000,
            direction: "rtl"
          });
        });
      })
      .catch(error => {
        this.snackBar.open("עידכון בית הספר נכשל", undefined, {
          duration: 3000,
          direction: "rtl"
        });
      });
  }

  deleteSchool() {
    this.snackBar
      .open("מחיקה של בית ספר מוחק גם את כל נתוניו?!?", "אישור", {
        duration: 8000,
        direction: "rtl"
      })
      .onAction()
      .subscribe(action => {
        this.schoolsService.deleteSchool(this.schoolToUpdate);
        this.dialogRef.close();
      });
  }
}
