import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSchoolFormComponent } from './edit-school-form.component';

describe('EditSchoolFormComponent', () => {
  let component: EditSchoolFormComponent;
  let fixture: ComponentFixture<EditSchoolFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSchoolFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSchoolFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
