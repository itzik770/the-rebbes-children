import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { School } from 'src/app/models/School';

@Component({
  selector: 'app-school-form',
  templateUrl: './school-form.component.html',
  styleUrls: ['./school-form.component.css']
})
export class SchoolFormComponent implements OnInit {

  @Input() school: School;
  @Output() submitForm: EventEmitter<School>;
  schoolForm: FormGroup;

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<SchoolFormComponent>
    ) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    this.schoolForm = this.fb.group({
      id: new FormControl(this.school.id),
      name: new FormControl(this.school.name, [Validators.required]),
      writingPermission: new FormControl(this.school.writingPermission || ''),
      classes: new FormControl(this.school.classes || []),
      city: new FormControl(this.school.city, [Validators.required]),
      campaigns: new FormControl(this.school.campaigns || [])
    });
  }

  onSubmit() {
    this.school = { ...this.school, ...this.schoolForm.value};
    this.dialogRef.close();
    this.submitForm.emit(this.school);
  }
}
