import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchoolFormComponent } from './add-school-form.component';

describe('AddSchoolFormComponent', () => {
  let component: AddSchoolFormComponent;
  let fixture: ComponentFixture<AddSchoolFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSchoolFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSchoolFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
