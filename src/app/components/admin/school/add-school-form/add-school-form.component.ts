import { Component, OnInit } from '@angular/core';
import { School } from 'src/app/models/School';
import { SchoolsService } from 'src/app/services/schools.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-school-form',
  templateUrl: './add-school-form.component.html',
  styleUrls: ['./add-school-form.component.css']
})
export class AddSchoolFormComponent implements OnInit {

  newSchool = {} as School;
  allSchools: School[] = [];
  constructor(private snackBar: MatSnackBar, private schoolsService: SchoolsService) { }

  ngOnInit() {
  }

  async addSchool(schoolToAdd: School) {
    let snackBarMessage = '';
    try {
      await this.getAllSchools();
      if (this.checkIfSchoolExist(schoolToAdd)) {
        snackBarMessage = `בית הספר ${schoolToAdd.name} כבר קיים!`;
      } else {
        await this.schoolsService.addSchool(schoolToAdd);
        snackBarMessage = 'בית הספר נוסף בהצלחה';
      }
      this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
    } catch (error) {
      this.snackBar.open('הוספת הבית ספר נכשלה', undefined, { duration: 3000, direction: 'rtl' });
    }
  }

  checkIfSchoolExist(schoolToAdd: School) {
    const schoolsData = this.orginizeSchoolData();
    return schoolsData.includes(`${schoolToAdd.name} ${schoolToAdd.city}`)
  }

  orginizeSchoolData() {
    const schoolsData = [];
    for (const schoolData of this.allSchools) {
      schoolsData.push(`${schoolData.name} ${schoolData.city}`);
    }
    return schoolsData;
  }

  async getAllSchools() {
    const schoolData = await this.schoolsService.getSchools();
    schoolData.map(school => {
      const id = school.payload.doc.id;
      const data = school.payload.doc.data();
      this.allSchools.push({ id, ...data });
    });
  }
}
