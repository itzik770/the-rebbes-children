import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Manager } from 'src/app/models/Manager';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: "app-manager-form",
  templateUrl: "./manager-form.component.html",
  styleUrls: ["./manager-form.component.css"]
})
export class ManagerFormComponent implements OnInit {
  @Input() manager: Manager;
  @Output() submitForm: EventEmitter<Manager>;
  managerForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ManagerFormComponent>
  ) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    this.managerForm = this.fb.group({
      id: new FormControl(this.manager.id),
      firebaseUid: new FormControl(this.manager.firebaseUid, [Validators.required]),
      schoolId: new FormControl(this.manager.schoolId, [Validators.required]),
      storeId: new FormControl(this.manager.storeId, [Validators.required]),
      firstName: new FormControl(this.manager.firstName, [Validators.required]),
      lastName: new FormControl(this.manager.lastName, [Validators.required]),
      phoneNumber: new FormControl(this.manager.phoneNumber, [Validators.required]),
      managerCode: new FormControl(this.manager.managerCode, [Validators.required]),
      classCounter: new FormControl(this.manager.classCounter || '10'),
      studentCounter: new FormControl(this.manager.studentCounter || '100'),
      productCounter: new FormControl(this.manager.productCounter || 10),
      campaignCounter: new FormControl(this.manager.campaignCounter || '10'),
      email: new FormControl(this.manager.email, [Validators.required]),
      createDate: new FormControl(this.manager.createDate || new Date().toISOString()),
      writingPermission: new FormControl(this.manager.writingPermission || {})
    });
  }

  onSubmit() {
    this.manager = { ...this.manager, ...this.managerForm.value };
    this.dialogRef.close();
    this.submitForm.emit(this.manager);
  }
}
