import { Component, OnInit } from '@angular/core';
import { Manager } from 'src/app/models/Manager';
import { MatSnackBar } from '@angular/material';
import { ManagersService } from 'src/app/services/managers.service';
import { School } from 'src/app/models/School';

@Component({
  selector: 'app-add-manager-form',
  templateUrl: './add-manager-form.component.html',
  styleUrls: ['./add-manager-form.component.css']
})
export class AddManagerFormComponent implements OnInit {
  newManager = {} as Manager;
  allTheManager: Manager[] = [];
  constructor(private snackBar: MatSnackBar, private managerService: ManagersService) { }

  ngOnInit() {
  }

  async addManager(managerToAdd: Manager) {
    let snackBarMessage = '';
    try {
      await this.getAllManagers();
      if (this.checkIfManagerExist(managerToAdd)) {
        snackBarMessage = `השם ${managerToAdd.firstName} ${managerToAdd.lastName} כבר קיים במערכת`;
      } else {
        await this.managerService.addManager(managerToAdd);
        // await this.schoolsService.addSchool(schoolToAdd);
        snackBarMessage = 'המנהל נוסף בהצלחה';
      }
      this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
    } catch (error) {
      this.snackBar.open('הוספת המנהל נכשלה', undefined, { duration: 3000, direction: 'rtl' });
    }
  }

  checkIfManagerExist(managerToAdd: Manager) {
    const managersData = this.orginizeManagerData();
    return managersData.includes(`${managerToAdd.firstName} ${managerToAdd.lastName} ${managerToAdd.email} ${managerToAdd.firebaseUid}`)
  }

  orginizeManagerData() {
    const managersData = [];
    for (const managerData of this.allTheManager) {
      managersData.push(`${managerData.firstName} ${managerData.lastName} ${managerData.email} ${managerData.firebaseUid}`);
    }
    return managersData;
  }

  async getAllManagers() {
    const managersData = await this.managerService.getManagers();
    managersData.map(manager => {
      const id = manager.payload.doc.id;
      const data = manager.payload.doc.data();
      this.allTheManager.push({ id, ...data });
    });
  }
}
