import { Component, OnInit, Inject } from "@angular/core";
import { Manager } from 'src/app/models/Manager';
import { ManagersService } from 'src/app/services/managers.service';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';

@Component({
  selector: "app-edit-manager-form",
  templateUrl: "./edit-manager-form.component.html",
  styleUrls: ["./edit-manager-form.component.css"]
})
export class EditManagerFormComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public managerToUpdate: Manager,
    private snackBar: MatSnackBar,
    private managerService: ManagersService,
    private dialogRef: MatDialogRef<EditManagerFormComponent>
  ) {}

  ngOnInit() {}

  updateManager(aManager: Manager) {
    this.managerService.updateManager(aManager)
      .then(() => {
        this.dialogRef.beforeClosed().subscribe(() => {
          this.snackBar.open("עידכון פרטי המנהל הסתיים בהצלחה", undefined, {
            duration: 3000,
            direction: "rtl"
          });
        });
      })
      .catch(error => {
        this.snackBar.open("עידכון המנהל נכשל", undefined, {
          duration: 3000,
          direction: "rtl"
        });
      });
  }

  deleteManager() {
    this.snackBar
      .open("מחיקה של מנהל מוחק גם את כל נתוניו?!?", "אישור", {
        duration: 8000,
        direction: "rtl"
      })
      .onAction()
      .subscribe(action => {
        this.managerService.deleteManager(this.managerToUpdate);
        this.dialogRef.close();
      });
  }
}
