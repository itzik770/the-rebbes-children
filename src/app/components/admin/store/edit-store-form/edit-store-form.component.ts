import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef } from '@angular/material';
import { Store } from 'src/app/models/Store';
import { StoresService } from 'src/app/services/stores.service';

@Component({
  selector: 'app-edit-store-form',
  templateUrl: './edit-store-form.component.html',
  styleUrls: ['./edit-store-form.component.css']
})
export class EditStoreFormComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public storeToUpdate: Store,
    private snackBar: MatSnackBar,
    private storeService: StoresService,
    private dialogRef: MatDialogRef<EditStoreFormComponent>
  ) {}

  ngOnInit() {}

  updateStore(aStore: Store) {
    this.storeService.updateStore(aStore)
      .then(() => {
        this.dialogRef.beforeClosed().subscribe(() => {
          this.snackBar.open("עידכון פרטי המנהל הסתיים בהצלחה", undefined, {
            duration: 3000,
            direction: "rtl"
          });
        });
      })
      .catch(error => {
        this.snackBar.open("עידכון המנהל נכשל", undefined, {
          duration: 3000,
          direction: "rtl"
        });
      });
  }

  deleteStore() {
    this.snackBar
      .open("מחיקה של מנהל מוחק גם את כל נתוניו?!?", "אישור", {
        duration: 8000,
        direction: "rtl"
      })
      .onAction()
      .subscribe(action => {
        this.storeService.deleteStore(this.storeToUpdate);
        this.dialogRef.close();
      });
  }
}
