import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Store } from 'src/app/models/Store';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-store-form',
  templateUrl: './store-form.component.html',
  styleUrls: ['./store-form.component.css']
})
export class StoreFormComponent implements OnInit {

  @Input() store: Store;
  @Output() submitForm: EventEmitter<Store>;
  storeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<StoreFormComponent>
  ) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    this.storeForm = this.fb.group({
      id: new FormControl(this.store.id, [Validators.required]),
      name: new FormControl(this.store.name, [Validators.required]),
      products: new FormControl(this.store.products || []),
      amountOfProducts: new FormControl(this.store.amountOfProducts || 0),
      createDate: new FormControl(this.store.createDate || new Date().toISOString()),
      expirationDate: new FormControl(this.store.expirationDate || new Date(2050, 1, 1).toISOString()),
      writingPermission: new FormControl(this.store.writingPermission || {})
    });
  }

  onSubmit() {
    this.store = { ...this.store, ...this.storeForm.value };
    this.dialogRef.close();
    this.submitForm.emit(this.store);
  }
}
