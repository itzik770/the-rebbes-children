import { Component, OnInit } from '@angular/core';
import { Store } from 'src/app/models/Store';
import { MatSnackBar } from '@angular/material';
import { StoresService } from 'src/app/services/stores.service';

@Component({
  selector: 'app-add-store-form',
  templateUrl: './add-store-form.component.html',
  styleUrls: ['./add-store-form.component.css']
})
export class AddStoreFormComponent implements OnInit {

  newStore = {} as Store;
  allTheStore: Store[] = [];
  constructor(private snackBar: MatSnackBar, private storeService: StoresService) { }

  ngOnInit() {
  }

  async addStore(storeToAdd: Store) {
    let snackBarMessage = '';
    try {
      await this.getAllStores();
      if (this.checkIfStoreExist(storeToAdd)) {
        snackBarMessage = `הקיוסק ${storeToAdd.name} כבר קיים במערכת`;
      } else {
        await this.storeService.addStore(storeToAdd);
        snackBarMessage = 'הקיוסק נוסף בהצלחה';
      }
      this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
    } catch (error) {
      this.snackBar.open('הוספת הקיוסק נכשלה', undefined, { duration: 3000, direction: 'rtl' });
    }
  }

  checkIfStoreExist(storeToAdd: Store) {
    const storesData = this.orginizeStoreData();
    return storesData.includes(`${storeToAdd.name} ${storeToAdd.id}`)
  }

  orginizeStoreData() {
    const storesData = [];
    for (const storeData of this.allTheStore) {
      storesData.push(`${storeData.name} ${storeData.id}`);
    }
    return storesData;
  }

  async getAllStores() {
    const storesData = await this.storeService.getStores();
    storesData.map(store => {
      const id = store.payload.doc.id;
      const data = store.payload.doc.data();
      this.allTheStore.push({ id, ...data });
    });
  }
}
