import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
// import { HomePageComponent } from '../home-page/home-page.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMessage = '';
  showSpinner = false;

  constructor(
    public authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<LoginComponent>
  ) { }
  ngOnInit() {
    for (let index = 0; index < 100; index++) {
      clearInterval(index);      
    }
    
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  tryLogin(emailAddress: string, password: string) {
    console.log('emailAddress', emailAddress);
    console.log('password', password);
    
    this.showSpinner = true;
    this.authService.doLogin(emailAddress, password)
      .then(res => {
        this.showSpinner = false;
        this.dialogRef.close(!this.showSpinner);
        // this.router.navigate(['/manager_info']);
      }, err => {
        console.log(err);
        this.showSpinner = false;
        this.dialogRef.close(this.showSpinner);
        this.errorMessage = err.message;
      });
  }

}
