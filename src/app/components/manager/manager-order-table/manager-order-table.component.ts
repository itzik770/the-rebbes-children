import { Component, OnInit, ContentChild, Inject } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Order } from 'src/app/models/Order';
import { Student } from 'src/app/models/Student';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { DocumentChangeAction } from 'angularfire2/firestore';
// import { PaginationService } from 'src/app/services/pagination.service';

@Component({
    selector: 'app-manager-order-table',
    templateUrl: './manager-order-table.component.html',
    styleUrls: ['./manager-order-table.component.css']
})
export class ManagerOrderTableComponent implements OnInit {

    displayedColumns: string[] = ['complete', 'productPicUrl', 'details', 'purchasePrice', 'purchaseDate'];
    dataSource: MatTableDataSource<Order>;
    @ContentChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ContentChild(MatSort, { static: true }) sort: MatSort;

    studentCampaignOrders: Order[] = [];
    student: Student;
    showStudent = false;
    studentOrders: Order[];
    STORAGE_KEY = 'student_info';
    studentOrdersTemp: DocumentChangeAction<Order>[];
    isLoading: boolean = false;

    constructor(private orderService: OrdersService, @Inject(LOCAL_STORAGE) public storage: StorageService) { }

    async ngOnInit() {
        this.fetchStudent();
    }

    async fetchStudent() {
        this.student = await this.storage.get(this.STORAGE_KEY);
        if (Object.entries(this.student).length === 0 && this.student.constructor === Object) {
            this.showStudent = false;
        } else {
            this.getAllStudentCampaignOrders(this.student.id);
            this.showStudent = true;
        }
    }

    async organizeTableData() {
        this.dataSource = new MatTableDataSource(this.studentCampaignOrders);
    }

    async getAllStudentCampaignOrders(studentId: string) {
        const tempOrderData = await this.orderService
            .getFirstOrdersUnderStudentManagerView(studentId, 'purchaseDate', 11);

        this.studentOrdersTemp = tempOrderData;

        tempOrderData.map(async orderData => {
            const data = orderData.payload.doc.data();
            const id = orderData.payload.doc.id;
            this.studentCampaignOrders.push({ id, ...data });
        });
        this.organizeTableData()
    }

    async loadMore() {
        if (!this.isLoading) {
            this.isLoading = true;
            const tempDoc = this.studentOrdersTemp[this.studentCampaignOrders.length - 1].payload.doc;
            const tempOrderData = await this.orderService.getMore(this.student.id, 'purchaseDate', 11, tempDoc);
            tempOrderData.map(async orderData => {
                this.studentOrdersTemp.push(orderData);
                const data = orderData.payload.doc.data();
                const id = orderData.payload.doc.id;
                this.studentCampaignOrders.push({ id, ...data });
            });
            this.isLoading = false;
            this.dataSource.data = this.studentCampaignOrders;
        }
    }

    scrollHandler(e) {
        console.log('manager-order-table', e)
        if (e === 'bottom') {
            this.loadMore();
        }
        // should log top or bottom
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
}
