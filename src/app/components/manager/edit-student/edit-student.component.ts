import { Component, OnInit, Inject, NgZone, ViewChild } from '@angular/core';
import { Order } from 'src/app/models/Order';
import { Score } from 'src/app/models/Score';
import { Student } from 'src/app/models/Student';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { OrdersService } from 'src/app/services/orders.service';
import { ClassesService } from 'src/app/services/classes.service';
import { StudentsService } from 'src/app/services/students.service';
import { StudentCampaignData } from 'src/app/models/StudentCampaignData';
import { AddScoreComponent } from '../../score/add-score/add-score.component';
import { EditStudentFormComponent } from '../../student/edit-student-form/edit-student-form.component';
import { ManagerScoreTableComponent } from '../manager-score-table/manager-score-table.component';
import { isUndefined } from 'util';
import { SelectStudentFirstComponent } from '../../error-msgs/select-student-first/select-student-first.component';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {

  showStudent = false;
  studentOrders: Order[];
  studentScores: Score[];
  STORAGE_KEY = 'student_info';
  student: Student = {} as Student;
  @ViewChild(ManagerScoreTableComponent, { static: false }) scoreComponent: ManagerScoreTableComponent;

  constructor(@Inject(LOCAL_STORAGE) public storage: StorageService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private orderService: OrdersService,
    private classesService: ClassesService,
    private studentService: StudentsService,
    public ngZone: NgZone,
    public router: Router) { }

  ngOnInit() {
    this.fetchStudent();
  }
  async fetchStudent() {
    this.student = await this.storage.get(this.STORAGE_KEY);
    // console.log('fetchStudent', this.student);

    // this.student.campaignScores[0].earnedPoints
    if (isUndefined(this.student) || (Object.entries(this.student).length === 0 && this.student.constructor === Object)) {
      const dialogRef = this.dialog.open(SelectStudentFirstComponent);
      setInterval(() => {
        dialogRef.close();
        dialogRef.afterClosed().subscribe(() => {
          this.ngZone.run(() => {
            this.router.navigate(['/manager_info']);
          });
        });
      }, 2000);
      this.showStudent = false;
    } else {
      this.showStudent = true;
    }
  }

  async getAllStudentOrders() {
    this.studentOrders = [];
    this.studentOrders = await this.orderService.getOrdersUnderStudent(this.student.id);
  }

  async openAddScoreModal() {
    let newScore = {} as Score;
    const studentAndCampaign: StudentCampaignData = {
      'student': this.student,
      'campaignId': this.student.campaignScores[0].campaignId,
      'campaignName': this.student.campaignScores[0].campaignName
    };
    const dialogRef = this.dialog.open(AddScoreComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: studentAndCampaign
      });

    dialogRef.beforeClosed().subscribe(() => {
      newScore = dialogRef.componentInstance.newScore;
    })
    dialogRef.afterClosed().subscribe(async () => {
      this.scoreComponent.addScoreToTable(newScore);
      setTimeout(async () => {
        await this.syncStudentInfo(this.student.id);
      }, 1000);
    });
  }

  openEditStudentModal() {
    const dialogRef = this.dialog.open(EditStudentFormComponent,
      {
        data: this.student
      });
    dialogRef.afterClosed().subscribe(async () => {
      // console.log('kakai');
      setTimeout(() => {
        this.fetchStudent();
      }, 500);
      // this.syncStudentInfo(this.student.id);
      // const studentData = await this.studentService.getStudentById(this.student.id).toPromise();
      // this.student = studentData.payload.data();
      // this.student.id = studentData.payload.id;
    })
  }

  async syncStudentInfo(studentId: string) {
    const studentData = await this.studentService.getStudentById(studentId).toPromise();
    this.student = studentData.payload.data();
    this.student.id = studentData.payload.id;
    this.storage.set(this.STORAGE_KEY, this.student);
  }

  async deleteStudent() {
    this.snackBar.open('מחיקה של תלמיד מוחקת את כל נתוניו! האם אתה בטוח?', 'אישור', { duration: 8000, direction: 'rtl' })
      .onAction()
      .subscribe(action => {
        // this.dialogRef.close();
        this.studentService.deleteStudent(this.student.id);
        this.classesService.removeStudentFromClass(this.student);
        this.ngZone.run(() => {
          this.router.navigate(['/manager_info']);
        });
      });
  }
}

