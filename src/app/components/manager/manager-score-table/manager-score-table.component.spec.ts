import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerScoreTableComponent } from './manager-score-table.component';

describe('ManagerScoreTableComponent', () => {
  let component: ManagerScoreTableComponent;
  let fixture: ComponentFixture<ManagerScoreTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerScoreTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerScoreTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
