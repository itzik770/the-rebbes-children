import { Component, OnInit, ContentChild, Input, Inject } from "@angular/core";
import { MatTableDataSource, MatPaginator, MatSort, MatSnackBar } from "@angular/material";
import { Score } from "src/app/models/Score";
import { Student } from "src/app/models/Student";
import { ScoreService } from "src/app/services/score.service";
import { LOCAL_STORAGE, StorageService } from "ngx-webstorage-service";
// import { PaginationService } from 'src/app/services/pagination.service';
import { Observable } from "rxjs/Observable";
import { take } from "rxjs/operators";
import { DocumentChangeAction } from "angularfire2/firestore";
import { StudentsService } from 'src/app/services/students.service';

@Component({
  selector: "app-manager-score-table",
  templateUrl: "./manager-score-table.component.html",
  styleUrls: ["./manager-score-table.component.css"]
})
export class ManagerScoreTableComponent implements OnInit {
  displayedColumns: string[] = [
    "scoreAmount",
    "details",
    "scoreDate",
    "actions"
  ];
  dataSource: MatTableDataSource<Score>;
  @ContentChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ContentChild(MatSort, { static: true }) sort: MatSort;

  studentCampaignScores: Score[] = [];
  student: Student;
  showStudent = false;
  studentScores: Score[];
  STORAGE_KEY = "student_info";
  studentScoresTemp: DocumentChangeAction<Score>[];
  isLoading: boolean = false;

  constructor(
    private scoreService: ScoreService,
    private studentsService: StudentsService,
    private snackBar: MatSnackBar,
    @Inject(LOCAL_STORAGE) public storage: StorageService
  ) {}

  async ngOnInit() {
    this.fetchStudent();
  }

  async fetchStudent() {
    this.student = await this.storage.get(this.STORAGE_KEY);
    if (
      Object.entries(this.student).length === 0 &&
      this.student.constructor === Object
    ) {
      this.showStudent = false;
    } else {
      this.getAllStudentCampaignScores(this.student.id);
      this.showStudent = true;
    }
  }

  async organizeTableData() {
    this.dataSource = new MatTableDataSource(this.studentCampaignScores);
  }

  async getAllStudentCampaignScores(studentId: string) {
    const tempScoreData = await this.scoreService.getFirstScoresUnderStudentManagerView(
      studentId,
      "scoreDate",
      11
    );
    this.studentScoresTemp = tempScoreData;
    tempScoreData.map(async scoreData => {
      const data = scoreData.payload.doc.data();
      const id = scoreData.payload.doc.id;
      this.studentCampaignScores.push({ id, ...data });
    });
    this.organizeTableData();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  async loadMore() {
    if (!this.isLoading) {
      this.isLoading = true;
      const tempDoc = this.studentScoresTemp[
        this.studentCampaignScores.length - 1
      ].payload.doc;
      const tempOrderData = await this.scoreService.getMore(
        this.student.id,
        "scoreDate",
        11,
        tempDoc
      );
      tempOrderData.map(async scoreData => {
        this.studentScoresTemp.push(scoreData);
        const data = scoreData.payload.doc.data();
        const id = scoreData.payload.doc.id;
        this.studentCampaignScores.push({ id, ...data });
      });
      this.isLoading = false;
      this.dataSource.data = this.studentCampaignScores;
    }
  }

  scrollHandler(e) {
    console.log("manager-score-table", e);
    if (e === "bottom") {
      this.loadMore();
    }
    // should log top or bottom
  }

  async deleteScore(scoreToDelete: Score) {
      console.log("scoreToDelete", scoreToDelete);
    this.snackBar
      .open("אתה בטוח שאתה רוצה למחוק את הניקוד?", "אישור", {
        duration: 8000,
        direction: "rtl"
      })
      .onAction()
      .subscribe(action => {
        this.scoreService.deleteScore(scoreToDelete.id);
        this.studentsService.subStudentScore(scoreToDelete.studentId, scoreToDelete.campaignId, scoreToDelete.scoreAmount);
        console.log("this.studentCampaignScores", this.studentCampaignScores);
    
        this.studentCampaignScores = this.studentCampaignScores.filter(
          score => score.id !== scoreToDelete.id
        );
        console.log("this.studentCampaignScores", this.studentCampaignScores);
        this.dataSource.data = this.studentCampaignScores;
      });

  }

  async addScoreToTable(newScore: Score) {
    this.studentCampaignScores.unshift(newScore);
    this.dataSource._updateChangeSubscription();
  }
}
