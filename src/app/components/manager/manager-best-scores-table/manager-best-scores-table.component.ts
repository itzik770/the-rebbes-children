import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatTableDataSource } from "@angular/material";
import { Manager } from "src/app/models/Manager";
import { StudentsService } from "src/app/services/students.service";
import { Student } from "src/app/models/Student";
import { ClassesService } from "src/app/services/classes.service";
import { Class } from "src/app/models/Class";
import { DocumentChangeAction } from 'angularfire2/firestore';

@Component({
  selector: "app-manager-best-scores-table",
  templateUrl: "./manager-best-scores-table.component.html",
  styleUrls: ["./manager-best-scores-table.component.css"]
})
export class ManagerBestScoresTableComponent implements OnInit {
  studentsArray: Student[] = [];
  classesArray: Class[] = [];
  displayedColumns: string[] = [
    "className",
    "firstName",
    "lastName",
    "earnedPoints"
  ];
  dataSource: MatTableDataSource<Student>;
  isLoading: boolean = false;
  studentTemp: DocumentChangeAction<Student>[];


  constructor(
    @Inject(MAT_DIALOG_DATA) public manager: Manager,
    private studentsService: StudentsService,
    private classesService: ClassesService
  ) {}

  ngOnInit() {
    this.getTopStudents(this.manager.schoolId);
  }

  async getAllClassesUnderSchool(schoolId: string) {
    const classesData = await this.classesService.getClassesBySchoolId(
      schoolId
    );
    classesData.map(classData => {
      const id = classData.payload.doc.id;
      const data = classData.payload.doc.data();
      this.classesArray.push({ id, ...data });
    });
  }

  async getTopStudents(schoolId: string) {
    await this.getAllClassesUnderSchool(this.manager.schoolId);
    const tempStudentsData = await this.studentsService.getTopStudentsManagerView(
      schoolId,
      "earnedPoints",
      15
    );
    tempStudentsData.map(async studentData => {
      const data = studentData.payload.doc.data();
      const id = studentData.payload.doc.id;
      const classInfo = this.classesArray.filter(
        aClass => aClass.id === data.classId
      )[0];
      data.classId = classInfo.name;
      this.studentsArray.push({ id, ...data });
    });
    this.organizeTableData();
  }

  async organizeTableData() {
    this.dataSource = new MatTableDataSource(this.studentsArray);
  }

  // async loadMore() {
  //   if (!this.isLoading) {
  //     this.isLoading = true;
  //     const tempDoc = this.studentTemp[this.studentsArray.length - 1].payload.doc;
  //     const tempOrderData = await this.orderService.getMore(
  //       this.student.id,
  //       "purchaseDate",
  //       11,
  //       tempDoc
  //     );
  //     tempOrderData.map(async orderData => {
  //       this.studentOrdersTemp.push(orderData);
  //       const data = orderData.payload.doc.data();
  //       const id = orderData.payload.doc.id;
  //       this.studentCampaignOrders.push({ id, ...data });
  //     });
  //     this.isLoading = false;
  //     this.dataSource.data = this.studentCampaignOrders;
  //   }
  // }

  // scrollHandler(e) {
  //   console.log("manager-order-table", e);
  //   if (e === "bottom") {
  //     this.loadMore();
  //   }
  //   // should log top or bottom
  // }
}
