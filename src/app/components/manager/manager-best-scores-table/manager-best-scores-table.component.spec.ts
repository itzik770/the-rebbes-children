import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerBestScoresTableComponent } from './manager-best-scores-table.component';

describe('ManagerBestScoresTableComponent', () => {
  let component: ManagerBestScoresTableComponent;
  let fixture: ComponentFixture<ManagerBestScoresTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagerBestScoresTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerBestScoresTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
