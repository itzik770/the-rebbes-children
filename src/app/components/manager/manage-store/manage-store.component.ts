import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddProductFormComponent } from '../../product/add-product-form/add-product-form.component';
import { StorageService, LOCAL_STORAGE } from 'ngx-webstorage-service';
import { Manager } from 'src/app/models/Manager';
import { Product } from 'src/app/models/Product';
import { ProductsService } from 'src/app/services/products.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-manage-store',
  templateUrl: './manage-store.component.html',
  styleUrls: ['./manage-store.component.css']
})
export class ManageStoreComponent implements OnInit {
  STORAGE_KEY = 'manager_info';
  manager: Manager;
  products: Product[] = [];
  numberOfProducts: number;
  showSpinner = true;

  constructor(private dialog: MatDialog,
    @Inject(LOCAL_STORAGE) public storage: StorageService,
    private snackBar: MatSnackBar,
    private productsService: ProductsService) { }

  ngOnInit() {
    this.manager = this.storage.get(this.STORAGE_KEY);
    this.getAllStoreProducts(this.manager.storeId);
  }

  openAddProductModal() {
    const dialogRef = this.dialog.open(AddProductFormComponent, {
      maxWidth: '80vw',
      maxHeight: '80vh',
      data: this.manager
    });

    dialogRef.afterClosed().subscribe(() => {
      setTimeout(() => {
        this.getAllStoreProducts(this.manager.storeId);
        this.showSpinner = false
      }, 3000);
      this.showSpinner = true
      // const { hey } = product;
      // console.log('product', product);
      // console.log('hey', hey);

      // this.products.push(product);
    });
  }

  deleteProduct(productToDelete: Product) {
    this.snackBar.open('מחיקה של מוצר היא סופית ללא חזרה, האם אתה בטוח?!', 'אישור', { duration: 8000, direction: 'rtl' })
      .onAction()
      .subscribe(action => {
        this.productsService.deleteProduct(productToDelete).then(after => {
          // this.getAllStoreProducts(this.manager.storeId);
          this.products = this.products.filter(product => product.id !== productToDelete.id);
        });
      });
  }

  async getAllStoreProducts(storeId: string) {
    this.products = [];
    const productsData = await this.productsService.getAllProductsUnderStoreManager(storeId);
    this.showSpinner = false;
    productsData.map(async productData => {
      const data = productData.payload.doc.data();
      const id = productData.payload.doc.id;
      this.products.push({ id, ...data });
    });
    this.numberOfProducts = this.products.length;
    // console.log(this.numberOfProducts);
    
  }
}
