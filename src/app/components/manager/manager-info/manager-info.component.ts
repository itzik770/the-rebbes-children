import { Component, OnInit, Input, Inject, NgZone } from '@angular/core';
import { SchoolsService } from 'src/app/services/schools.service';
import { Observable } from 'rxjs';
import { School } from 'src/app/models/School';
import { ClassesService } from 'src/app/services/classes.service';
import { Class } from 'src/app/models/Class';
import { Student } from 'src/app/models/Student';
import { StudentsService } from 'src/app/services/students.service';
import { DocumentChangeAction } from 'angularfire2/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Manager } from 'src/app/models/Manager';
import { take } from 'rxjs/operators';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Router } from '@angular/router';
import { EditClassFormComponent } from '../../class/edit-class-form/edit-class-form.component';
import { AddClassFormComponent } from '../../class/add-class-form/add-class-form.component';
import { AddStudentFormComponent } from '../../student/add-student-form/add-student-form.component';
import { StudentList } from 'src/app/models/StudentList';
import { AddCampaignFormComponent } from '../../campaign/add-campaign-form/add-campaign-form.component';
import { ManagerBestScoresTableComponent } from '../manager-best-scores-table/manager-best-scores-table.component';

@Component({
  selector: 'app-manager-info',
  templateUrl: './manager-info.component.html',
  styleUrls: ['./manager-info.component.css']
})
export class ManagerInfoComponent implements OnInit {

  @Input() class: Class;
  showSpinner = true;
  showClassesSpinner = true;
  showStudentsSpinner = false;
  schoolId: string;
  school: School;
  classes: Observable<DocumentChangeAction<Class>[]>;
  classesNames: string[] = [];
  selectedClass: Class;
  allClasses: Class[] = [];
  studentOfTheClass: StudentList[] = [];
  manager: Manager;
  studentInfo: Student = {} as Student;
  manager_key = 'manager_info';
  student_key = 'student_info';
  constructor(private schoolsService: SchoolsService,
    private classesService: ClassesService,
    private studentsService: StudentsService,
    private dialog: MatDialog,
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    public ngZone: NgZone,
    public router: Router) { }

  ngOnInit() {
    this.manager = this.storage.get(this.manager_key) || {};
    // console.log('this.manager', this.manager);
    
    if (this.manager.id) {
      this.getSchoolData();
    }
  }

  async getSchoolData() {
    this.schoolId = this.manager.schoolId;
    this.showSpinner = false;

    this.school = await this.schoolsService.getSchoolById(this.schoolId).pipe(take(1)).toPromise();
    this.getAllClasses(this.manager.schoolId);
  }

  async sortClasses() {
    await this.allClasses.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
  }

  async sortStudens() {
    await this.studentOfTheClass.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
  }

  async getAllClasses(schoolId: string) {
    this.showClassesSpinner = true;
    this.allClasses = [];
    this.studentOfTheClass = [];
    const classesData = await this.classesService.getClassesBySchoolId(schoolId);
    classesData.map(async classData => {
      const data = classData.payload.doc.data();
      const id = classData.payload.doc.id;
      this.allClasses.push({ id, ...data });
    });
    this.sortClasses();
    this.showClassesSpinner = false;
  }


  async getClassInfo(passedClass: Class) {
    try {
      this.showStudentsSpinner = true;
      this.studentOfTheClass = [];
      const classData = await this.classesService.getClassById(passedClass.id);
      this.selectedClass = classData.payload.data();
      this.selectedClass.id = classData.payload.id;
      // console.log('this.selectedClass.studentList', this.selectedClass.studentList);
      
      if (this.selectedClass.studentList.length) {
        this.selectedClass.studentList.map(studentData => {
          this.studentOfTheClass.push(studentData)
        });
        this.sortStudens();
      }
      this.showStudentsSpinner = false;

    } catch (error) {
      console.log(error);
    }
    // this.showStudentsSpinner = true;
  }

  openBestStudentsTable() {
    this.dialog.open(ManagerBestScoresTableComponent, {
      maxWidth: '80vw',
        maxHeight: '80vh',
        data: this.manager
    });
  }

  // TODO limit puling scores and orders of students.
  openClassModalToEdit(passedClass: Class) {
    this.dialog.open(EditClassFormComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: passedClass
      }).afterClosed().subscribe(() => {
        this.getAllClasses(this.schoolId);
      });
  }
  openAddClassModal() {
    const dialogRef = this.dialog.open(AddClassFormComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: this.manager
      });
    let newClassToDialog: Class = {} as Class;

    dialogRef.beforeClosed().subscribe(() => {
      newClassToDialog = dialogRef.componentInstance.newClass;
    });
    dialogRef.afterClosed().subscribe(() => {
      if (!(Object.entries(newClassToDialog).length === 0 && newClassToDialog.constructor === Object)) {
        this.allClasses.push(newClassToDialog);
      }
    });
  }

  openAddStudentModal() {
    try {
      const dialogRef = this.dialog.open(AddStudentFormComponent, {
        maxWidth: '80vw',
        maxHeight: '80vh',
        data: { 'managerInfo': this.manager, 'parentId': this.selectedClass.id }
      });
      let newStudentToDialog: Student = {} as Student;
      dialogRef.beforeClosed().subscribe(() => {
        newStudentToDialog = dialogRef.componentInstance.student;
      });
      dialogRef.afterClosed().subscribe(() => {
        if (!(Object.entries(newStudentToDialog).length === 0 && newStudentToDialog.constructor === Object)) {
          setTimeout(() => {
            this.getClassInfo(this.selectedClass);
          }, 2000)
          // this.studentOfTheClass.push({ id: newStudentToDialog.id, name: `${newStudentToDialog.firstName} ${newStudentToDialog.lastName}` });
          // this.studentOfTheClass.push(newStudentToDialog);
          // this.selectedClass.studentList.push({ id: newStudentToDialog.id, name: `${newStudentToDialog.firstName} ${newStudentToDialog.lastName}` });
        }
      });
    } catch (error) {
      console.log(error);

    }
  }

  async openStudentModalToEdit(passedStudentId: string) {
    // console.log('passedStudentId', passedStudentId);

    const studentPromise = await this.studentsService.getStudentById(passedStudentId).toPromise();
    const student = studentPromise.payload.data();
    student.id = studentPromise.payload.id;
    this.studentInfo = Object.assign({}, student);
    this.storage.set(this.student_key, this.studentInfo);
    this.ngZone.run(() => {
      this.router.navigate(['/edit_student_info']);
    });
  }

  openAddCampaignModal() {
    const dialogRef = this.dialog.open(AddCampaignFormComponent,
      {
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: this.manager
      });
  }

}
