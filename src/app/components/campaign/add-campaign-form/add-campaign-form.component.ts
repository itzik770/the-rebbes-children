import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Manager } from 'src/app/models/Manager';
import { Campaign } from 'src/app/models/Campaign';
import { CampaignsService } from 'src/app/services/campaigns.service';
import { SchoolsService } from 'src/app/services/schools.service';
import { ManagersService } from 'src/app/services/managers.service';
import { StudentsService } from 'src/app/services/students.service';

@Component({
  selector: 'app-add-campaign-form',
  templateUrl: './add-campaign-form.component.html',
  styleUrls: ['./add-campaign-form.component.css']
})
export class AddCampaignFormComponent implements OnInit {

  newCampaign: Campaign = {} as Campaign;
  allCampaigns: Campaign[] = [];
  campaignNames: string[];
  constructor(private snackBar: MatSnackBar,
    private managersService: ManagersService,
    private schoolsService: SchoolsService,
    private studentService: StudentsService,
    private campaignsService: CampaignsService,
    private dialogRef: MatDialogRef<AddCampaignFormComponent>,
    @Inject(MAT_DIALOG_DATA) public managerInfo: Manager
    ) { }

  ngOnInit() {
    this.getAllCampaignsUnderSchool();
  }

  getAllCampaignsUnderSchool() {
    this.campaignsService.getCampaignsUnderSchoolId(this.managerInfo.schoolId)
      .subscribe(campaignsData => {
        campaignsData.map(campaignData => {
          const data = campaignData.payload.doc.data();
          const id = campaignData.payload.doc.id;
          this.allCampaigns.push({ id, ...data });
        });
      });
  }


  async addCampaign(newCampaign: Campaign) {
    let snackBarMessage: string;
    try {
      const campaignNames = [];
      for (const campaignData of this.allCampaigns) {
        await campaignNames.push(campaignData.name);
      }
      if (campaignNames.includes(newCampaign.name)) {
        snackBarMessage = `הקמפיין ${newCampaign.name} כבר קיים!`;
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      } else {
        newCampaign.schoolId = this.managerInfo.schoolId;
        const campaignId = await this.campaignsService.addCampaign(newCampaign, this.managerInfo);
        await this.schoolsService.addCampaignToSchool(this.managerInfo.schoolId, newCampaign);
        await this.studentService.addCampaignToStudentsUnderSchool(campaignId, newCampaign.name, this.managerInfo.schoolId);
        this.managersService.increaseCampaignCounter(this.managerInfo.id);
        snackBarMessage = `הקמפיין ${newCampaign.name} נוסף בהצלחה!`;
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      }
      this.dialogRef.beforeClosed().subscribe(() => {
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      });
    } catch (error) {
      this.snackBar.open('הוספת הקמפיין נכשל', undefined, { duration: 3000, direction: 'rtl' });
    }
  }
}
