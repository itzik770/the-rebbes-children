import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Campaign } from 'src/app/models/Campaign';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-campaign-form',
  templateUrl: './campaign-form.component.html',
  styleUrls: ['./campaign-form.component.css']
})
export class CampaignFormComponent implements OnInit {

  @Input() campaign: Campaign;
  @Output() submitForm: EventEmitter<Campaign>;
  campaignForm: FormGroup;

  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<CampaignFormComponent>
    ) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    this.campaignForm = this.fb.group({
      name: new FormControl(this.campaign.name, [Validators.required]),
      description: new FormControl(this.campaign.description || ''),
      schoolId: new FormControl(this.campaign.schoolId),
      createDate: new FormControl(this.campaign.createDate || new Date().toISOString()),
      expirationDate: new FormControl(this.campaign.expirationDate || new Date(2025, 1, 1).toISOString())
    });
  }

  onSubmit() {
    this.campaign = { ...this.campaign, ...this.campaignForm.value};
    this.dialogRef.close();
    this.submitForm.emit(this.campaign);
  }

}
