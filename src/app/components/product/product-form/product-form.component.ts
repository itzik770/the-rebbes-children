import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Manager } from 'src/app/models/Manager';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  @Input() product: Product;
  @Input() managerInfo: Manager;
  @Output() submitForm: EventEmitter<Product>;

  @Output() downloadUrl = new EventEmitter<string>();
  productForm: FormGroup;
  storeOrStudent = 'store';
  constructor(private fb: FormBuilder,
    private dialogRef: MatDialogRef<ProductFormComponent>,
  ) {
    this.submitForm = new EventEmitter();
  }
  ngOnInit() {
    this.productForm = this.fb.group({
      name: new FormControl(this.product.name || '', [Validators.required]),
      price: new FormControl(this.product.price || '', [Validators.required]),
      storeId: new FormControl(this.product.storeId || this.managerInfo.storeId),
      productPicture: new FormControl(this.product.productPicture),
      numberOfPurchase: new FormControl(this.product.numberOfPurchase)
    });
  }

  onDownloadUrl(url: string) {
    this.productForm.value.productPicture = url;
  }
  onSubmit() {
    this.product = { ...this.product, ...this.productForm.value };
    this.dialogRef.close();
    this.submitForm.emit(this.product);
  }
}
