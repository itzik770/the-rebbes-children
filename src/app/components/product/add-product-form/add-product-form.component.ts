import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Product } from 'src/app/models/Product';
import { ProductsService } from 'src/app/services/products.service';
import { ManagersService } from 'src/app/services/managers.service';
import { Manager } from 'src/app/models/Manager';

@Component({
  selector: 'app-add-product-form',
  templateUrl: './add-product-form.component.html',
  styleUrls: ['./add-product-form.component.css']
})
export class AddProductFormComponent implements OnInit {
  allProducts: Product[] = [];
  product = {} as Product;
  storeId = '';

  constructor(private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<AddProductFormComponent>,
    private managersService: ManagersService,
    private productsService: ProductsService,
    @Inject(MAT_DIALOG_DATA) public managerInfo: Manager) { }

  ngOnInit() {
    this.getProductsUnderStoreId();
  }

  async getProductsUnderStoreId() {
    const productsData = await this.productsService.getAllProductsUnderStore(this.managerInfo.storeId);
    productsData.map(async productData => {
      const data = productData.payload.doc.data();
      const id = productData.payload.doc.id;
      this.allProducts.push({ id, ...data });
    });
  }
  async addProduct(productToAdd: Product) {
    let snackBarMessage: string;
    try {
      const productsNames = [];
      for (const productData of this.allProducts) {
        productsNames.push(`${productData.name}`);
      }
      if (productsNames.includes(`${productToAdd.name}`)) {
        snackBarMessage = `המוצר ${productToAdd.name} כבר קיים בקיוסק!`;
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      } else {
        const productRef = await this.productsService.addProduct(productToAdd, this.managerInfo);
        // this.classesService.addStudentToClass(studentRef, this.importantInfo.parentId);
        this.managersService.increaseProductCounter(this.managerInfo.id);
        snackBarMessage = `המוצר ${productToAdd.name} נוסף בהצלחה!`;
        this.allProducts.push(productToAdd);
        // this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      }
      this.dialogRef.beforeClosed().subscribe(() => {
        this.snackBar.open(snackBarMessage, undefined, { duration: 3000, direction: 'rtl' });
      });
    } catch (error) {
      this.snackBar.open('הוספת המוצר נכשלה', undefined, { duration: 3000, direction: 'rtl' });
    }
  }
}
