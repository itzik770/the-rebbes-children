import { Component, OnInit, Inject } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { Order } from 'src/app/models/Order';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Manager } from 'src/app/models/Manager';
import { Student } from 'src/app/models/Student';
import { ClassesService } from 'src/app/services/classes.service';
import { Class } from 'src/app/models/Class';
// import { _ } from 'underscore';
import { OrderToShow } from 'src/app/models/OrderToShow';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  STORAGE_KEY = 'manager_info';
  unfinishedOrders: Order[] = [];
  unfinishedStudents: Student[] = [];
  classesInfo: Class[] = [];
  manager: Manager;
  classesWithStudents: OrderToShow[] = [];
  showSpinner = true;

  constructor(@Inject(LOCAL_STORAGE) public storage: StorageService,
    private snackBar: MatSnackBar,
    private classesService: ClassesService,
    private ordersService: OrdersService,
  ) { }

  ngOnInit() {
    this.manager = this.storage.get(this.STORAGE_KEY);
    this.organizeOrdersAndClassesInfo();
  }

  async organizeOrdersAndClassesInfo() {
    await this.organizeOrders();
    await this.organizeClassesInfo();
    await this.sortAndOrder();
  }

  async organizeOrders() {
    const unfinishedOrdersData = await this.ordersService.getAllUnfinishedOrdersBySchool(this.manager.schoolId);
    this.showSpinner = false;
    unfinishedOrdersData.map(async orderData => {
      const data = orderData.payload.doc.data();
      const id = orderData.payload.doc.id;
      this.unfinishedOrders.push({ id, ...data });
    });
  }

  async organizeClassesInfo() {
    const classesInfoData = await this.classesService.getClassesBySchoolId(this.manager.schoolId);
    classesInfoData.map(async (classData) => {
      const data = classData.payload.doc.data();
      const id = classData.payload.doc.id;
      this.classesInfo.push({ id, ...data });
    });
  }

  snakebarConfirm(order: Order, className: string) {
    // console.log('snakebarConfirm', order, className);
    this.snackBar.open('האם אתה בטוח?', 'אישור', { duration: 8000, direction: 'rtl' })
      .onAction()
      .subscribe(action => {
        this.ordersService.markOrderAsComplete(order.id).then(() => {
          this.classesWithStudents.map(aClass => {
            if (aClass.className === className) {
              aClass.orders = aClass.orders.filter(aOrder => aOrder.id !== order.id);
            }
          });
        });
      });
  }
  async sortAndOrder() {
    // let classesWithIdAndOrders: object;
    // classesWithIdAndOrders = await _.groupBy(this.unfinishedOrders, function (order: Order) {
    //   return order.classId;
    // });
    // for (const classId in classesWithIdAndOrders) {
    //   if (classesWithIdAndOrders.hasOwnProperty(classId)) {
    //     const theClass = this.classesInfo.filter(aClass => aClass.id === classId)[0];
    //     const className = theClass.name;
    //     const orders: Order[] = classesWithIdAndOrders[classId];
    //     orders.sort((a, b) => a.studentName.localeCompare(b.studentName));
    //     const orderToShow = {
    //       className: className,
    //       orders: orders
    //     };
    //     this.classesWithStudents.push(orderToShow);
    //   }
    // }
  }
}
