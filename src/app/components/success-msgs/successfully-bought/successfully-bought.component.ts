import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { PassToStore } from 'src/app/models/PassToStore';

@Component({
  selector: 'app-successfully-bought',
  templateUrl: './successfully-bought.component.html',
  styleUrls: ['./successfully-bought.component.css']
})
export class SuccessfullyBoughtComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public infoForBuying: PassToStore) { }

  ngOnInit() {
    
  }

}
