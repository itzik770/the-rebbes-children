import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFirestoreModule, FirestoreSettingsToken } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirePerformanceModule } from '@angular/fire/performance';
import { environment } from '../environments/environment';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { StudentInfoComponent } from './components/student/student-info/student-info.component';
import 'hammerjs';
import { SchoolsService } from './services/schools.service';
import { ClassesService } from './services/classes.service';
import { StudentsService } from './services/students.service';
// import { ManagerInfoComponent } from './components/manager-info/manager-info.component';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { AddClassFormComponent } from './components/class/add-class-form/add-class-form.component';
import { EditClassFormComponent } from './components/class/edit-class-form/edit-class-form.component';
import { ClassFormComponent } from './components/class/class-form/class-form.component';
import { LoaderSpinnerComponent } from './components/loader-spinner/loader-spinner.component';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';
import { UserResolver } from './components/user/user.resolver';
import { AuthGuard } from './auth-guard';
import { StudentAuthGuardGuard } from './student-auth-guard.guard';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AddStudentFormComponent } from './components/student/add-student-form/add-student-form.component';
import { EditStudentFormComponent } from './components/student/edit-student-form/edit-student-form.component';
import { StudentFormComponent } from './components/student/student-form/student-form.component';
import { AngularFireStorage, AngularFireStorageModule } from 'angularfire2/storage';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { DropZoneDirective } from './directives/drop-zone.directive';
import { FileSizePipe } from './pipes/file-size.pipe';
import { StoreComponent } from './components/store/store/store.component';
import { AddProductFormComponent } from './components/product/add-product-form/add-product-form.component';
import { EditProductFormComponent } from './components/product/edit-product-form/edit-product-form.component';
import { ProductFormComponent } from './components/product/product-form/product-form.component';
import { AddCampaignFormComponent } from './components/campaign/add-campaign-form/add-campaign-form.component';
import { CampaignFormComponent } from './components/campaign/campaign-form/campaign-form.component';
import { EditCampaignFormComponent } from './components/campaign/edit-campaign-form/edit-campaign-form.component';
import { OrdersService } from './services/orders.service';
import { ScoreService } from './services/score.service';
import { OrderTableComponent } from './components/student/order-table/order-table.component';
import { ScoreTableComponent } from './components/student/score-table/score-table.component';
import { AddScoreComponent } from './components/score/add-score/add-score.component';
import { ScoreFormComponent } from './components/score/score-form/score-form.component';
import { WrongUserErrorComponent } from './components/error-msgs/wrong-user-error/wrong-user-error.component';
import { StorageServiceModule } from 'ngx-webstorage-service';
// import { ManageStoreComponent } from './components/store/manage-store/manage-store.component';
import { NotEnoughMoneyComponent } from './components/error-msgs/not-enough-money/not-enough-money.component';
import { ConfirmBuyingComponent } from './components/store/confirm-buying/confirm-buying.component';
import { NotYourCardComponent } from './components/error-msgs/not-your-card/not-your-card.component';
import { SuccessfullyBoughtComponent } from './components/success-msgs/successfully-bought/successfully-bought.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MainNavigationBarComponent } from './components/navs/main-navigation-bar/main-navigation-bar.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ManagerInfoComponent } from './components/manager/manager-info/manager-info.component';
import { EditStudentComponent } from './components/manager/edit-student/edit-student.component';
import { ManageStoreComponent } from './components/manager/manage-store/manage-store.component';
import { ManagerScoreTableComponent } from './components/manager/manager-score-table/manager-score-table.component';
import { ManagerOrderTableComponent } from './components/manager/manager-order-table/manager-order-table.component';
// import { AdminComponent } from './components/admin/admin.component';
import { ScrollableDirective } from './directives/scrollable.directive';
import { SelectStudentFirstComponent } from './components/error-msgs/select-student-first/select-student-first.component';
import { SchoolFormComponent } from './components/admin/school/school-form/school-form.component';
import { AddSchoolFormComponent } from './components/admin/school/add-school-form/add-school-form.component';
import { EditSchoolFormComponent } from './components/admin/school/edit-school-form/edit-school-form.component';
import { AdminComponent } from './components/admin/admin/admin.component';
import { ManagerFormComponent } from './components/admin/manager/manager-form/manager-form.component';
import { AddManagerFormComponent } from './components/admin/manager/add-manager-form/add-manager-form.component';
import { EditManagerFormComponent } from './components/admin/manager/edit-manager-form/edit-manager-form.component';
import { StoreFormComponent } from './components/admin/store/store-form/store-form.component';
import { AddStoreFormComponent } from './components/admin/store/add-store-form/add-store-form.component';
import { EditStoreFormComponent } from './components/admin/store/edit-store-form/edit-store-form.component';
import { ManagerBestScoresTableComponent } from './components/manager/manager-best-scores-table/manager-best-scores-table.component';
// import { ManagerTableScoreComponent } from './components/manager/manager-table-score/manager-table-score.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavigationBarComponent,
    HomePageComponent,
    LoaderSpinnerComponent,
    AdminComponent,
    StudentInfoComponent,
    ManagerScoreTableComponent,
    ManagerOrderTableComponent,
    RegisterComponent,
    ManagerInfoComponent,
    NotFoundComponent,
    AddClassFormComponent,
    AddStudentFormComponent,
    EditStudentFormComponent,
    StudentFormComponent,
    UserComponent,
    LoginComponent,
    EditClassFormComponent,
    ClassFormComponent,
    FileSizePipe,
    EditStudentComponent,
    DropZoneDirective,
    ScrollableDirective,
    FileUploadComponent,
    OrdersComponent,
    StoreComponent,
    AddProductFormComponent,
    EditProductFormComponent,
    ProductFormComponent,
    AddCampaignFormComponent,
    EditCampaignFormComponent,
    CampaignFormComponent,
    WrongUserErrorComponent,
    ManageStoreComponent,
    OrderTableComponent,
    NotEnoughMoneyComponent,
    SelectStudentFirstComponent,
    ConfirmBuyingComponent,
    NotYourCardComponent,
    SuccessfullyBoughtComponent,
    ScoreTableComponent,
    AddScoreComponent,
    ScoreFormComponent,
    SchoolFormComponent,
    AddSchoolFormComponent,
    EditSchoolFormComponent,
    ManagerFormComponent,
    AddManagerFormComponent,
    EditManagerFormComponent,
    StoreFormComponent,
    ManagerBestScoresTableComponent,
    AddStoreFormComponent,
    EditStoreFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirePerformanceModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    MaterialModule,
    BrowserAnimationsModule,
    StorageServiceModule,
  ],
  providers: [{ provide: FirestoreSettingsToken, useValue: {} }, AngularFireStorage, OrdersService, ScoreService,
    FormBuilder, SchoolsService, ClassesService, StudentsService, AuthService, UserResolver, AuthGuard, StudentAuthGuardGuard],
  entryComponents: [
    StudentInfoComponent,
    NotEnoughMoneyComponent,
    SelectStudentFirstComponent,
    ConfirmBuyingComponent,
    AddClassFormComponent,
    EditClassFormComponent,
    AddStudentFormComponent,
    EditStudentFormComponent,
    SchoolFormComponent,
    ManagerBestScoresTableComponent,
    AddSchoolFormComponent,
    EditSchoolFormComponent,
    NotYourCardComponent,
    SuccessfullyBoughtComponent,
    LoginComponent,
    AddCampaignFormComponent,
    EditCampaignFormComponent,
    AddProductFormComponent,
    EditProductFormComponent,
    ManagerFormComponent,
    AddManagerFormComponent,
    EditManagerFormComponent,
    StoreFormComponent,
    AddStoreFormComponent,
    EditStoreFormComponent,
    ProductFormComponent,
    AddScoreComponent,
    WrongUserErrorComponent,
    ScoreFormComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
