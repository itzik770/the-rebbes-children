import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { RegisterComponent } from './components/register/register.component';
import { AuthGuard } from './auth-guard';
import { UserComponent } from './components/user/user.component';
import { UserResolver } from './components/user/user.resolver';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { StoreComponent } from './components/store/store/store.component';
import { StudentInfoComponent } from './components/student/student-info/student-info.component';
import { StudentAuthGuardGuard } from './student-auth-guard.guard';
import { OrdersComponent } from './components/orders/orders.component';
import { ManagerInfoComponent } from './components/manager/manager-info/manager-info.component';
import { EditStudentComponent } from './components/manager/edit-student/edit-student.component';
import { ManageStoreComponent } from './components/manager/manage-store/manage-store.component';
import { AdminComponent } from './components/admin/admin/admin.component';
// import { AdminComponent } from './components/admin/admin.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'store', component: StoreComponent, canActivate: [StudentAuthGuardGuard] },
  { path: 'personal_area', component: StudentInfoComponent , canActivate: [StudentAuthGuardGuard]},
  { path: 'manager_info', component: ManagerInfoComponent, resolve: { data: UserResolver } },
  { path: 'edit_student_info', component: EditStudentComponent, resolve: { data: UserResolver } },
  { path: 'manage_store', component: ManageStoreComponent, resolve: { data: UserResolver } },
  { path: 'distribution_list', component: OrdersComponent, resolve: { data: UserResolver } },
  { path: 'user', component: UserComponent, resolve: { data: UserResolver } },
  { path: 'admin', component: AdminComponent, resolve: { data: UserResolver } },
  { path: '**', component: NotFoundComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
