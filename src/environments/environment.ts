// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    // apiKey: "AIzaSyBg6yDVmUXKH_rt-bGNF0vUeNVbL0h0ppQ",
    // authDomain: "rebbechildrens-prod.firebaseapp.com",
    // databaseURL: "https://rebbechildrens-prod.firebaseio.com",
    // projectId: "rebbechildrens-prod",
    // storageBucket: "rebbechildrens-prod.appspot.com",
    // messagingSenderId: "505533273164",
    // appId: "1:505533273164:web:ee5051c06a52aad13b90e9",
    // measurementId: "G-VFEPRCMX6B"
    apiKey: "AIzaSyC95pU13Z3mP0GrfSFW5WXPekOO4IFtSPQ",
    authDomain: "rebbechildrens-dev.firebaseapp.com",
    databaseURL: "https://rebbechildrens-dev.firebaseio.com",
    projectId: "rebbechildrens-dev",
    storageBucket: "rebbechildrens-dev.appspot.com",
    messagingSenderId: "861691932320",
    appId: "1:861691932320:web:b889942bc8a55aef8663c5",
    measurementId: "G-F8WJXHEQX7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
