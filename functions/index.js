const functions = require('firebase-functions').region('europe-west1');
const admin = require('firebase-admin');
admin.initializeApp();

exports.aggregateScores = functions
  .firestore
  .document(`scores/{scoreId}`)
  .onCreate(async event => {
    try {
      const scoreId = event.id;
      const docRef = admin.firestore().collection('scores').doc(scoreId);
      const scoreData = await docRef.get();
      const scoreRealData = scoreData.data();
      const studentId = scoreRealData.studentId;
      const studentRef = admin.firestore().collection('students').doc(studentId);
      const studentData = await studentRef.get();
      const recentScores = [];
      const collectionRef = admin.firestore().collection('scores');
      const querySnapshot = await collectionRef.where('studentId', '==', studentId)
        .orderBy('scoreDate', 'desc')
        .limit(5)
        .get();
      querySnapshot.forEach(doc => {
        const score = doc.data();
        score["id"] = doc.id;
        recentScores.push(score);
      });
      const student = studentData.data();
      student.recentScores = recentScores;
      studentRef.update(student);
    } catch (error) {
      console.log(error);
    }
  });

  exports.aggregateOrders = functions.firestore
  .document(`orders/{orderId}`)
  .onCreate(async event => {
    try {
      const orderId = event.id;
      const docRef = admin.firestore().collection('orders').doc(orderId);
      const orderData = await docRef.get();
      const orderRealData = orderData.data();
      // console.log('orderRealData', orderRealData);
      const studentId = orderRealData.studentId;
      const studentRef = admin.firestore().collection('students').doc(studentId);
      const studentData = await studentRef.get();
      // console.log('studentData.data()', studentData.data());
      const recentOrders = [];
      const collectionRef = admin.firestore().collection('orders');
      const querySnapshot = await collectionRef.where('studentId', '==', studentId)
        .orderBy('purchaseDate', 'desc')
        .limit(5)
        .get();
      querySnapshot.forEach(doc => {
        const order = doc.data();
        order["id"] = doc.id;
        recentOrders.push(order);
        // console.log('recentOrders', recentOrders)
      });
      const student = studentData.data();
      student.recentOrders = recentOrders;
      studentRef.update(student);
      // console.log('bla', bla);
    } catch (error) {
      console.log(error);
    }
  });

  exports.deleteFromAggregateProductsInStore = functions.firestore
  .document(`products/{productId}`)
  .onDelete(async event => {
    try {
      let storeId = '';
      if (!event.after.exists) {
        // On Delete
        const productPreviousData = event.before.data();
        storeId = productPreviousData.storeId;
      } else {
        const productId = event.after.id;
        const docRef = admin.firestore().collection('products').doc(productId);
        const productData = await docRef.get();
        const productRealData = productData.data();
        storeId = productRealData.storeId;
      }
      const storeRef = admin.firestore().collection('stores').doc(storeId);
      const storeData = await storeRef.get();
      const productsUnderStore = [];
      const collectionRef = admin.firestore().collection('products');
      const querySnapshot = await collectionRef
        .where('storeId', '==', storeId)
        .get();
      querySnapshot.forEach(doc => {
        const product = doc.data();
        product["id"] = doc.id;
        productsUnderStore.push(product);
      });
      const store = storeData.data();
      store.products = productsUnderStore;
      storeRef.update(store);
    } catch (error) {
      console.log(error);
    }
  });

exports.addToAggregateProductsToStore = functions.firestore
  .document(`products/{productId}`)
  .onCreate(async event => {
    try {
      let storeId = '';
      if (!event.after.exists) {
        // On Delete
        const productPreviousData = event.before.data();
        storeId = productPreviousData.storeId;
      } else {
        const productId = event.after.id;
        const docRef = admin.firestore().collection('products').doc(productId);
        const productData = await docRef.get();
        const productRealData = productData.data();
        storeId = productRealData.storeId;
      }
      const storeRef = admin.firestore().collection('stores').doc(storeId);
      const storeData = await storeRef.get();
      const productsUnderStore = [];
      const collectionRef = admin.firestore().collection('products');
      const querySnapshot = await collectionRef
        .where('storeId', '==', storeId)
        .get();
      querySnapshot.forEach(doc => {
        const product = doc.data();
        product["id"] = doc.id;
        productsUnderStore.push(product);
      });
      const store = storeData.data();
      store.products = productsUnderStore;
      storeRef.update(store);
    } catch (error) {
      console.log(error);
    }
  });



// async function generateFirestoreDailyBackup() {
//   const googleAuth = require('google-auth-library');
//   const client = new googleAuth.GoogleAuth({
//   //  await require('google-auth-library').auth.getClient({
//     scopes: [
//       'https://www.googleapis.com/auth/datastore',
//       'https://www.googleapis.com/auth/cloud-platform' // We need these scopes
//     ]
//   })
//   const timestamp = Date.now().toLocaleString(); // a nice way to name your folder
//   const BUCKET_NAME = `backup-data-daily`
//   const projectId = await googleAuth.auth.getProjectId();
//   const url = `https://firestore.googleapis.com/v1beta1/projects/${projectId}/databases/(default):exportDocuments`;
//   const backup_route = `gs://${BUCKET_NAME}/${timestamp}`;
//   try {
//     const result = await client.request({
//         url,
//         method: 'POST',
//         data: {
//           outputUriPrefix: backup_route,
//           // collectionsIds: [] // if you want to specify which collections to export, none means all
//         }
//       });
//       console.log(`Backup saved on folder on ${backup_route}. \n${result.status}`);
//   } catch (error) {
//     console.log(`Error Happend while trying to backup data. data: ${timestamp}`);
//   }
// }

// exports.automatedBackups = functions.pubsub
//     .schedule('0 0 * * *')
//     .timeZone('UTC+02:00')
//     .onRun(generateFirestoreDailyBackup);


// export { generateFirestoreDailyBackup, automatedBackups }
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
